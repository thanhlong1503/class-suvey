package com.uet.classsurvey.exception;

public enum Result {
	
	SUCCESS(1, "Success"),
	UNAUTHORIZED(401, "Unauthorized"),
	TOKEN_EXPIRE_TIME(401, "Token expire time"),
	BAD_REQUEST(400, "Bad request"),
	FORBIDDEN(403, "Forbidden"),
	NOT_FOUND(404, "Api not found"),
	METHOD_NOT_ALLOW(405, "Method not allow"),
	
	STUDENT_NOT_EXIST(902, "Transaction not existed"),
	LECTURER_NOT_EXIST(802, "Bank account not existed"),
	CUSTOMER_NOT_EXIST(404, "Customer not existed"),
	CUSTOMER_GROUP_NOT_EXIST(404, "Customer not existed"),
	PRODUCT_NOT_EXIST(404, "Product not existed"),
	USER_NOT_EXIST(404, "User not existed"),
	ROLE_NOT_EXIST(404, "Role not existed"),
	NAVIGATION_NOT_EXIST(404, "Navigation not existed"),
	ROLR_NAVIGATION_NOT_EXIST(404, "Role Navigation not existed"),
	
	STUDENT_IS_EXISTED(901, "Student is existed"),
	LECTURER_IS_EXISTED(801, "Lecturer is existed"),
	CUSTOMER_IS_EXISTED(400, "Customer is existed"),
	CUSTOMER_GROUP_IS_EXISTED(400, "Customer is existed"),
	PRODUCT_IS_EXISTED(400, "Product is existed"),
	USER_IS_EXISTED(400, "User is existed"),
	ROLE_IS_EXISTED(400, "Role is existed"),
	NAVIGATION_IS_EXISTED(400, "Navigation is existed"),
	ROLE_NAVIGATION_IS_EXISTED(400, "Role Navigation is existed"),
	
	VALIDATION(400, "Validation Failed"),
	
	CONFIRM_PASSWORD(400, "Password and confirm password is not match"),
	
	VALID_USER_FAIL(401, "Username or password is incorrect"),
	
	USERNAME_IS_EXISTED(400, "Username is existed"),
	
	AZ_ACCOUNT_IS_EXISTED(400, "Az account is existed"),
	
	ROLE_CODE_IS_EXISTED(400, "Role code is existed"),
	
	;
	
	private int code;
	private String message;
	
	Result(int code, String message){
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public boolean isSuccess() {
		return (this.code==1);
	}
	
	
	

}
