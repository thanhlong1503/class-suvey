package com.uet.classsurvey.exception;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.uet.classsurvey.endpoint.dto.common.ResponseData;

/**
 * @author LongTT
 * @created 16/08/2018
 * 
 * @modified
 * @modifier
 **/

@ControllerAdvice
@RestController
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	private Logger LOGGER = LoggerFactory.getLogger(RestResponseEntityExceptionHandler.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.web.servlet.mvc.method.annotation.
	 * ResponseEntityExceptionHandler#handleNoHandlerFoundException(org.
	 * springframework.web.servlet.NoHandlerFoundException,
	 * org.springframework.http.HttpHeaders, org.springframework.http.HttpStatus,
	 * org.springframework.web.context.request.WebRequest)
	 */
	@Override
	protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		LOGGER.error(ex.getMessage());
		ResponseData<String> responseData = new ResponseData<String>(Result.NOT_FOUND.getCode(),
				Result.NOT_FOUND.getMessage(), null);
		return new ResponseEntity<Object>(responseData, HttpStatus.NOT_FOUND);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.web.servlet.mvc.method.annotation.
	 * ResponseEntityExceptionHandler#handleHttpRequestMethodNotSupported(org.
	 * springframework.web.HttpRequestMethodNotSupportedException,
	 * org.springframework.http.HttpHeaders, org.springframework.http.HttpStatus,
	 * org.springframework.web.context.request.WebRequest)
	 */
	@Override
	protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		LOGGER.error(ex.getMessage());
		ResponseData<String> responseData = new ResponseData<String>(Result.METHOD_NOT_ALLOW.getCode(),
				Result.METHOD_NOT_ALLOW.getMessage(), null);
		return new ResponseEntity<Object>(responseData, HttpStatus.METHOD_NOT_ALLOWED);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.web.servlet.mvc.method.annotation.
	 * ResponseEntityExceptionHandler#handleMethodArgumentNotValid(org.
	 * springframework.web.bind.MethodArgumentNotValidException,
	 * org.springframework.http.HttpHeaders, org.springframework.http.HttpStatus,
	 * org.springframework.web.context.request.WebRequest)
	 */
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		LOGGER.error(ex.getMessage());
		List<String> listMessage = new ArrayList<>();
		try {
			if (ex.getBindingResult().hasFieldErrors()) {
				List<FieldError> listFielError = ex.getBindingResult().getFieldErrors();
				for (FieldError fieldError : listFielError) {
					listMessage.add(fieldError.getField() + ": " + fieldError.getDefaultMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			e.printStackTrace();
		}

		ResponseData<List<String>> responseData = new ResponseData<List<String>>(Result.VALIDATION.getCode(),
				Result.VALIDATION.getMessage(), listMessage);
		return new ResponseEntity<Object>(responseData, HttpStatus.BAD_REQUEST);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.web.servlet.mvc.method.annotation.
	 * ResponseEntityExceptionHandler#handleExceptionInternal(java.lang.Exception,
	 * java.lang.Object, org.springframework.http.HttpHeaders,
	 * org.springframework.http.HttpStatus,
	 * org.springframework.web.context.request.WebRequest)
	 */
	@Override
	protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		LOGGER.error(ex.getMessage());
		ResponseData<String> responseData = new ResponseData<String>(Result.BAD_REQUEST.getCode(),
				Result.BAD_REQUEST.getMessage(), null);
		return new ResponseEntity<Object>(responseData, HttpStatus.BAD_REQUEST);
	}

}
