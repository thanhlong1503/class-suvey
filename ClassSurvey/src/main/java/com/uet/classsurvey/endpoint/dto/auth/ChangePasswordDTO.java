package com.uet.classsurvey.endpoint.dto.auth;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.uet.classsurvey.common.util.Constants.ERROR_MESSAGE;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author LongTT
 *
 * @param <T>
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ChangePasswordDTO {

	
	private String userName;
	
	@NotNull(message = ERROR_MESSAGE.NOT_NULL)
	@Size(min = 5, message = ERROR_MESSAGE.LENG_PASSWORD)
	private String password;

	@NotNull(message = ERROR_MESSAGE.NOT_NULL)
	@Size(min = 5, message = ERROR_MESSAGE.LENG_PASSWORD)
	private String newPassword;
	
	@NotNull(message = ERROR_MESSAGE.NOT_NULL)
	@Size(min = 5, message = ERROR_MESSAGE.LENG_PASSWORD)
	private String confirmPassword;

}
