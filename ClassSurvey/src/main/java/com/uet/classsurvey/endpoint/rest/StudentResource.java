package com.uet.classsurvey.endpoint.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.validation.Valid;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.uet.classsurvey.common.util.JsonHelper;
import com.uet.classsurvey.common.util.Constants.RESPONSE_CODE;
import com.uet.classsurvey.common.util.Constants.RESPONSE_MESSAGE;
import com.uet.classsurvey.dao.entities.Student;
import com.uet.classsurvey.endpoint.dto.common.ResponseData;
import com.uet.classsurvey.endpoint.dto.student.StudentDTO;
import com.uet.classsurvey.exception.Result;
import com.uet.classsurvey.service.StudentService;

@Controller
@RequestMapping("/api/uet/class-survey")
public class StudentResource {

	@Autowired
	StudentService studentService;

	@PostMapping(value = "/students", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<ResponseData<Integer>> create(@Valid @RequestBody StudentDTO student) {
		ResponseData<Integer> response = new ResponseData<>();
		Result result = studentService.create(student);
		response.setCode(result.getCode());
		response.setMessage(result.getMessage());
		if (result.isSuccess()) {
			return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.CREATED);
		}
		return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.BAD_REQUEST);
	}

	@PutMapping(value = "/students", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<ResponseData<Integer>> update(@Valid @RequestBody StudentDTO Student) {
		ResponseData<Integer> response = new ResponseData<>();
		Result result = studentService.update(Student);
		response.setCode(result.getCode());
		response.setMessage(result.getMessage());
		if (result.isSuccess()) {
			return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.OK);
		}
		return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.BAD_REQUEST);
	}

	@DeleteMapping(value = "/students/{id}")
	public ResponseEntity<ResponseData<Integer>> deleteStudentById(@PathVariable("id") int id) {
		ResponseData<Integer> response = new ResponseData<>();
		Result result = studentService.delete(id);
		response.setCode(result.getCode());
		response.setMessage(result.getMessage());
		if (result.isSuccess()) {
			return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.OK);
		}
		return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.BAD_REQUEST);
	}

	@GetMapping(value = "/students/{id}")
	public ResponseEntity<ResponseData<Student>> findOneStudents(@PathVariable("id") int id) {
		ResponseData<Student> response = new ResponseData<>();
		Student Students = studentService.findOne(id);
		if (Students != null) {
			response.setCode(RESPONSE_CODE.SUCCESS_CODE);
			response.setMessage(RESPONSE_MESSAGE.SUCCESS_MSG);
			response.setData(Students);
			return new ResponseEntity<ResponseData<Student>>(response, HttpStatus.OK);
		}
		response.setCode(RESPONSE_CODE.ERR_CODE_BAD_REQUEST);
		response.setMessage(RESPONSE_MESSAGE.ERR_MSG_BAD_REQUEST);
		response.setData(Students);
		return new ResponseEntity<ResponseData<Student>>(response, HttpStatus.BAD_REQUEST);
	}

	@GetMapping(value = "/list/students", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<ResponseData<Object>> getAll() {
		ResponseData<Object> response = new ResponseData<>();
		List<Student> listStudents = studentService.getList();
		if (listStudents != null) {
			response.setCode(RESPONSE_CODE.SUCCESS_CODE);
			response.setMessage(RESPONSE_MESSAGE.SUCCESS_MSG);
			response.setData(listStudents);
			return new ResponseEntity<ResponseData<Object>>(response, HttpStatus.OK);
		}
		response.setCode(RESPONSE_CODE.ERR_CODE_BAD_REQUEST);
		response.setMessage(RESPONSE_MESSAGE.ERR_MSG_BAD_REQUEST);
		response.setData(listStudents);
		return new ResponseEntity<ResponseData<Object>>(response, HttpStatus.BAD_REQUEST);

	}

	@GetMapping(value = "/students", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<ResponseData<Object>> getPage(@RequestParam("search") String searchJson, Pageable pageable) {
		ResponseData<Object> response = new ResponseData<>();
		Student Student = JsonHelper.convertToObject(searchJson, Student.class);

		Page<Student> pageStudents = studentService.advanceSearch(Student, pageable);
		if (pageStudents != null) {
			response.setCode(RESPONSE_CODE.SUCCESS_CODE);
			response.setMessage(RESPONSE_MESSAGE.SUCCESS_MSG);
			response.setData(pageStudents);
			return new ResponseEntity<ResponseData<Object>>(response, HttpStatus.OK);
		}
		response.setCode(RESPONSE_CODE.ERR_CODE_BAD_REQUEST);
		response.setMessage(RESPONSE_MESSAGE.ERR_MSG_BAD_REQUEST);
		response.setData(pageStudents);
		return new ResponseEntity<ResponseData<Object>>(response, HttpStatus.BAD_REQUEST);

	}

	@PostMapping("students/uploadFile")
	public ResponseEntity<ResponseData<Integer>> uploadFile(@RequestParam("file") MultipartFile file) {
		ResponseData<Integer> response = new ResponseData<>();
		List<StudentDTO> listStudents = new ArrayList<>();

		try {
			XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
			XSSFSheet sheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = sheet.iterator();
			Row currentRow = iterator.next();
			while (iterator.hasNext()) {
				StudentDTO studentDTO = new StudentDTO();

				currentRow = iterator.next();
				Iterator<Cell> cellIterator = currentRow.iterator();

				Cell currentCell = cellIterator.next();

				if (!cellIterator.hasNext()) {
					break;
				}

				// get student code
				currentCell = cellIterator.next();
				studentDTO.setStudentCode(this.getValue(currentCell));

				// get password
				currentCell = cellIterator.next();
				studentDTO.setPassword(this.getValue(currentCell));

				// get fullname
				currentCell = cellIterator.next();
				studentDTO.setFullName(this.getValue(currentCell));

				// get email
				currentCell = cellIterator.next();
				studentDTO.setEmail(this.getValue(currentCell));

				// get address
				currentCell = cellIterator.next();
				studentDTO.setCourse(this.getValue(currentCell));

				if (studentDTO.getStudentCode() == null || studentDTO.getStudentCode().isEmpty()) {
					break;
				}

				listStudents.add(studentDTO);
			}
			// save
			Result result = studentService.create(listStudents);
			response.setCode(result.getCode());
			response.setMessage(result.getMessage());

			if (result.isSuccess()) {
				return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.CREATED);
			}
			return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.BAD_REQUEST);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.BAD_REQUEST);

	}

	private String getValue(Cell cell) {
		String str = "";
		try {
			if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING) {
				str = cell.getStringCellValue();
			} else if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC) {
				str = (int) cell.getNumericCellValue() + "";
			} else {
				str = cell.getStringCellValue();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return str;
	}

}
