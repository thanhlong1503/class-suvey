package com.uet.classsurvey.endpoint.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.validation.Valid;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.uet.classsurvey.common.util.JsonHelper;
import com.uet.classsurvey.common.util.Constants.RESPONSE_CODE;
import com.uet.classsurvey.common.util.Constants.RESPONSE_MESSAGE;
import com.uet.classsurvey.dao.entities.ClassSubject;
import com.uet.classsurvey.endpoint.dto.classsubject.ClassSubjectDTO;
import com.uet.classsurvey.endpoint.dto.common.ResponseData;
import com.uet.classsurvey.exception.Result;
import com.uet.classsurvey.service.ClassSubjectService;

@Controller
@RequestMapping("/api/uet/class-survey")
public class ClassSubjectResource {

	@Autowired
	ClassSubjectService classSubjectService;

	@PostMapping(value = "/class-subjects", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<ResponseData<Integer>> create(@Valid @RequestBody ClassSubject classSubject) {
		ResponseData<Integer> response = new ResponseData<>();
		Result result = classSubjectService.create(classSubject);
		response.setCode(result.getCode());
		response.setMessage(result.getMessage());
		if (result.isSuccess()) {
			return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.CREATED);
		}
		return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.BAD_REQUEST);
	}

	@PutMapping(value = "/class-subjects", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<ResponseData<Integer>> updateClassSubjects(@Valid @RequestBody ClassSubject classSubject) {
		ResponseData<Integer> response = new ResponseData<>();
		Result result = classSubjectService.update(classSubject);
		response.setCode(result.getCode());
		response.setMessage(result.getMessage());
		if (result.isSuccess()) {
			return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.OK);
		}
		return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.BAD_REQUEST);
	}

	@DeleteMapping(value = "/class-subjects/{id}")
	public ResponseEntity<ResponseData<Integer>> deleteClassSubjectById(@PathVariable("id") int id) {
		ResponseData<Integer> response = new ResponseData<>();
		Result result = classSubjectService.delete(id);
		response.setCode(result.getCode());
		response.setMessage(result.getMessage());
		if (result.isSuccess()) {
			return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.OK);
		}
		return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.BAD_REQUEST);
	}

	@GetMapping(value = "/class-subjects/{id}")
	public ResponseEntity<ResponseData<ClassSubject>> findOneClassSubjects(@PathVariable("id") int id) {
		ResponseData<ClassSubject> response = new ResponseData<>();
		ClassSubject classSubject = classSubjectService.findOne(id);
		if (classSubject != null) {
			response.setCode(RESPONSE_CODE.SUCCESS_CODE);
			response.setMessage(RESPONSE_MESSAGE.SUCCESS_MSG);
			response.setData(classSubject);
			return new ResponseEntity<ResponseData<ClassSubject>>(response, HttpStatus.OK);
		}
		response.setCode(RESPONSE_CODE.ERR_CODE_BAD_REQUEST);
		response.setMessage(RESPONSE_MESSAGE.ERR_MSG_BAD_REQUEST);
		response.setData(classSubject);
		return new ResponseEntity<ResponseData<ClassSubject>>(response, HttpStatus.BAD_REQUEST);
	}

	@GetMapping(value = "/list/class-subjects", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<ResponseData<Object>> getAll() {
		ResponseData<Object> response = new ResponseData<>();
		List<ClassSubject> listClassSubjects = classSubjectService.getList();
		if (listClassSubjects != null) {
			response.setCode(RESPONSE_CODE.SUCCESS_CODE);
			response.setMessage(RESPONSE_MESSAGE.SUCCESS_MSG);
			response.setData(listClassSubjects);
			return new ResponseEntity<ResponseData<Object>>(response, HttpStatus.OK);
		}
		response.setCode(RESPONSE_CODE.ERR_CODE_BAD_REQUEST);
		response.setMessage(RESPONSE_MESSAGE.ERR_MSG_BAD_REQUEST);
		response.setData(listClassSubjects);
		return new ResponseEntity<ResponseData<Object>>(response, HttpStatus.BAD_REQUEST);

	}

	@GetMapping(value = "/class-subjects", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<ResponseData<Object>> getPage(@RequestParam("search") String searchJson, Pageable pageable) {
		ResponseData<Object> response = new ResponseData<>();
		ClassSubject classSubject = JsonHelper.convertToObject(searchJson, ClassSubject.class);

		Page<ClassSubject> pageClassSubjects = classSubjectService.advanceSearch(classSubject, pageable);
		if (pageClassSubjects != null) {
			response.setCode(RESPONSE_CODE.SUCCESS_CODE);
			response.setMessage(RESPONSE_MESSAGE.SUCCESS_MSG);
			response.setData(pageClassSubjects);
			return new ResponseEntity<ResponseData<Object>>(response, HttpStatus.OK);
		}
		response.setCode(RESPONSE_CODE.ERR_CODE_BAD_REQUEST);
		response.setMessage(RESPONSE_MESSAGE.ERR_MSG_BAD_REQUEST);
		response.setData(pageClassSubjects);
		return new ResponseEntity<ResponseData<Object>>(response, HttpStatus.BAD_REQUEST);

	}

	@PostMapping("class-subjects/uploadFile")
	public ResponseEntity<ResponseData<Integer>> uploadFile(@RequestParam("file") MultipartFile file) {
		ResponseData<Integer> response = new ResponseData<>();
		ClassSubjectDTO classSubjectDTO = new ClassSubjectDTO();
		List<String> listStudentCode = new ArrayList<>();

		try {
			XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
			XSSFSheet sheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = sheet.iterator();
			iterator.next();
			iterator.next();
			iterator.next();
			iterator.next();
			// get time
			Row currentRow = iterator.next();
			Iterator<Cell> cellIterator = currentRow.iterator();
			Cell currentCell = cellIterator.next();
			String time = currentCell.getStringCellValue();
			classSubjectDTO.setTime(time);

			// get lecturer info
			currentRow = iterator.next();
			cellIterator = currentRow.iterator();
			// get lecturer name
			cellIterator.next();
			cellIterator.next();
			currentCell = cellIterator.next();
			String lecturerName = this.getValue(currentCell);

			// get lecturer code
			cellIterator.next();
			currentCell = cellIterator.next();
			classSubjectDTO.setLecturerCode(this.getValue(currentCell));

			// get time
			currentRow = iterator.next();
			cellIterator = currentRow.iterator();

			cellIterator.next();
			cellIterator.next();
			currentCell = cellIterator.next();
			classSubjectDTO.setSchedule(this.getValue(currentCell));

			// get classroom
			cellIterator.next();
			currentCell = cellIterator.next();
			classSubjectDTO.setClassroom(this.getValue(currentCell));

			// get classcode
			currentRow = iterator.next();
			cellIterator = currentRow.iterator();

			cellIterator.next();
			cellIterator.next();
			currentCell = cellIterator.next();
			classSubjectDTO.setName(this.getValue(currentCell));

			// get credit
			cellIterator.next();
			currentCell = cellIterator.next();
			classSubjectDTO.setCredits(Integer.parseInt(this.getValue(currentCell)));

			// get subject name
			currentRow = iterator.next();
			cellIterator = currentRow.iterator();

			cellIterator.next();
			cellIterator.next();
			currentCell = cellIterator.next();
			classSubjectDTO.setSubjectName(this.getValue(currentCell));

			// get data
			iterator.next();
			while (iterator.hasNext()) {

				currentRow = iterator.next();
				cellIterator = currentRow.iterator();
				if (!cellIterator.hasNext()) {
					break;
				}
				currentCell = cellIterator.next();

				// get studentcode
				currentCell = cellIterator.next();
				String studentCode = this.getValue(currentCell);

				if (studentCode == null || studentCode.isEmpty()) {
					break;
				}
				// add to list student code
				listStudentCode.add(studentCode);
				// get studentname
				currentCell = cellIterator.next();
				String studentName = this.getValue(currentCell);

			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		classSubjectDTO.setListStudentCode(listStudentCode);
		// save
		Result result = classSubjectService.create(classSubjectDTO);
		response.setCode(result.getCode());
		response.setMessage(result.getMessage());

		if (result.isSuccess()) {
			return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.CREATED);
		}
		return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.BAD_REQUEST);

	}

	private String getValue(Cell cell) {
		String str = "";
		try {
			if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING) {
				str = cell.getStringCellValue();
			} else if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC) {
				str = (int) cell.getNumericCellValue() + "";
			} else {
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return str;
	}

}
