package com.uet.classsurvey.endpoint.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uet.classsurvey.common.util.Constants.RESPONSE_CODE;
import com.uet.classsurvey.common.util.Constants.RESPONSE_MESSAGE;
import com.uet.classsurvey.dao.entities.FormSurvey;
import com.uet.classsurvey.endpoint.dto.common.ResponseData;
import com.uet.classsurvey.endpoint.dto.formsurvey.FormSurveyDTO;
import com.uet.classsurvey.exception.Result;
import com.uet.classsurvey.service.FormSurveyService;

@Controller
@RequestMapping("/api/uet/class-survey")
public class FormSurveyResource {

	@Autowired
	FormSurveyService formSurveyService;

	@PostMapping(value = "/form-survey", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<ResponseData<Integer>> create(@Valid @RequestBody FormSurveyDTO formSurvey) {
		ResponseData<Integer> response = new ResponseData<>();
		Result result = formSurveyService.create(formSurvey);
		response.setCode(result.getCode());
		response.setMessage(result.getMessage());
		if (result.isSuccess()) {
			return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.CREATED);
		}
		return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.BAD_REQUEST);
	}

	@PutMapping(value = "/form-survey", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<ResponseData<Integer>> update(@Valid @RequestBody FormSurveyDTO formSurvey) {
		ResponseData<Integer> response = new ResponseData<>();
		Result result = formSurveyService.update(formSurvey);
		response.setCode(result.getCode());
		response.setMessage(result.getMessage());
		if (result.isSuccess()) {
			return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.OK);
		}
		return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.BAD_REQUEST);
	}

	@DeleteMapping(value = "/form-survey/{id}")
	public ResponseEntity<ResponseData<Integer>> deleteFormSurveyById(@PathVariable("id") int id) {
		ResponseData<Integer> response = new ResponseData<>();
		Result result = formSurveyService.delete(id);
		response.setCode(result.getCode());
		response.setMessage(result.getMessage());
		if (result.isSuccess()) {
			return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.OK);
		}
		return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.BAD_REQUEST);
	}

	@GetMapping(value = "/form-survey/{id}")
	public ResponseEntity<ResponseData<FormSurvey>> findOneFormSurveys(@PathVariable("id") int id) {
		ResponseData<FormSurvey> response = new ResponseData<>();
		FormSurvey formSurvey = formSurveyService.findOne(id);
		if (formSurvey != null) {
			response.setCode(RESPONSE_CODE.SUCCESS_CODE);
			response.setMessage(RESPONSE_MESSAGE.SUCCESS_MSG);
			response.setData(formSurvey);
			return new ResponseEntity<ResponseData<FormSurvey>>(response, HttpStatus.OK);
		}
		response.setCode(RESPONSE_CODE.ERR_CODE_BAD_REQUEST);
		response.setMessage(RESPONSE_MESSAGE.ERR_MSG_BAD_REQUEST);
		response.setData(formSurvey);
		return new ResponseEntity<ResponseData<FormSurvey>>(response, HttpStatus.BAD_REQUEST);
	}

	@GetMapping(value = "/list/form-survey", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<ResponseData<Object>> getAll() {
		ResponseData<Object> response = new ResponseData<>();
		List<FormSurvey> listFormSurveys = formSurveyService.getList();
		if (listFormSurveys != null) {
			response.setCode(RESPONSE_CODE.SUCCESS_CODE);
			response.setMessage(RESPONSE_MESSAGE.SUCCESS_MSG);
			response.setData(listFormSurveys);
			return new ResponseEntity<ResponseData<Object>>(response, HttpStatus.OK);
		}
		response.setCode(RESPONSE_CODE.ERR_CODE_BAD_REQUEST);
		response.setMessage(RESPONSE_MESSAGE.ERR_MSG_BAD_REQUEST);
		response.setData(listFormSurveys);
		return new ResponseEntity<ResponseData<Object>>(response, HttpStatus.BAD_REQUEST);

	}

	@GetMapping(value = "/form-survey", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<ResponseData<Object>> getPage(Pageable pageable) {
		ResponseData<Object> response = new ResponseData<>();

		Page<FormSurvey> pageFormSurveys = formSurveyService.getPage(pageable);
		if (pageFormSurveys != null) {
			response.setCode(RESPONSE_CODE.SUCCESS_CODE);
			response.setMessage(RESPONSE_MESSAGE.SUCCESS_MSG);
			response.setData(pageFormSurveys);
			return new ResponseEntity<ResponseData<Object>>(response, HttpStatus.OK);
		}
		response.setCode(RESPONSE_CODE.ERR_CODE_BAD_REQUEST);
		response.setMessage(RESPONSE_MESSAGE.ERR_MSG_BAD_REQUEST);
		response.setData(pageFormSurveys);
		return new ResponseEntity<ResponseData<Object>>(response, HttpStatus.BAD_REQUEST);

	}

}
