package com.uet.classsurvey.endpoint.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uet.classsurvey.common.util.Constants.RESPONSE_CODE;
import com.uet.classsurvey.common.util.Constants.RESPONSE_MESSAGE;
import com.uet.classsurvey.dao.entities.Subject;
import com.uet.classsurvey.dao.entities.Survey;
import com.uet.classsurvey.endpoint.dto.common.ResponseData;
import com.uet.classsurvey.exception.Result;
import com.uet.classsurvey.service.SurveyService;

@Controller
@RequestMapping("/api/uet/class-survey")
public class SurveyResource {

	@Autowired
	private SurveyService surveyService;

	@PostMapping(value = "/surveys/generate", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<ResponseData<Integer>> generate() {
		ResponseData<Integer> response = new ResponseData<>();
		Result result = surveyService.generateSurvey();
		response.setCode(result.getCode());
		response.setMessage(result.getMessage());
		if (result.isSuccess()) {
			return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.CREATED);
		}
		return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.BAD_REQUEST);
	}

	@GetMapping(value = "/surveys", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<ResponseData<Object>> getPage(Pageable pageable) {
		ResponseData<Object> response = new ResponseData<>();

		Page<Survey> pageSurveys = surveyService.getPage(pageable);
		if (pageSurveys != null) {
			response.setCode(RESPONSE_CODE.SUCCESS_CODE);
			response.setMessage(RESPONSE_MESSAGE.SUCCESS_MSG);
			response.setData(pageSurveys);
			return new ResponseEntity<ResponseData<Object>>(response, HttpStatus.OK);
		}
		response.setCode(RESPONSE_CODE.ERR_CODE_BAD_REQUEST);
		response.setMessage(RESPONSE_MESSAGE.ERR_MSG_BAD_REQUEST);
		response.setData(pageSurveys);
		return new ResponseEntity<ResponseData<Object>>(response, HttpStatus.BAD_REQUEST);

	}

	@GetMapping(value = "/surveys/{id}")
	public ResponseEntity<ResponseData<Survey>> findOneSubjects(@PathVariable("id") int id) {
		ResponseData<Survey> response = new ResponseData<>();
		Survey survey = surveyService.findOne(id);
		if (survey != null) {
			response.setCode(RESPONSE_CODE.SUCCESS_CODE);
			response.setMessage(RESPONSE_MESSAGE.SUCCESS_MSG);
			response.setData(survey);
			return new ResponseEntity<ResponseData<Survey>>(response, HttpStatus.OK);
		}
		response.setCode(RESPONSE_CODE.ERR_CODE_BAD_REQUEST);
		response.setMessage(RESPONSE_MESSAGE.ERR_MSG_BAD_REQUEST);
		response.setData(survey);
		return new ResponseEntity<ResponseData<Survey>>(response, HttpStatus.BAD_REQUEST);
	}

	@GetMapping(value = "/surveys-of-student", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<ResponseData<Object>> getPageOfStudent(Pageable pageable) {
		ResponseData<Object> response = new ResponseData<>();

		Page<Survey> pageSurveys = surveyService.getPageOfStudent(pageable);
		if (pageSurveys != null) {
			response.setCode(RESPONSE_CODE.SUCCESS_CODE);
			response.setMessage(RESPONSE_MESSAGE.SUCCESS_MSG);
			response.setData(pageSurveys);
			return new ResponseEntity<ResponseData<Object>>(response, HttpStatus.OK);
		}
		response.setCode(RESPONSE_CODE.ERR_CODE_BAD_REQUEST);
		response.setMessage(RESPONSE_MESSAGE.ERR_MSG_BAD_REQUEST);
		response.setData(pageSurveys);
		return new ResponseEntity<ResponseData<Object>>(response, HttpStatus.BAD_REQUEST);

	}

}
