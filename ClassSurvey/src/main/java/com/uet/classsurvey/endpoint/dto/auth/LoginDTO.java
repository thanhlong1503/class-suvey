package com.uet.classsurvey.endpoint.dto.auth;

import javax.validation.constraints.NotNull;

import com.uet.classsurvey.common.util.Constants.ERROR_MESSAGE;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author LongTT
 *
 * @param <T>
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LoginDTO {

	@NotNull(message = ERROR_MESSAGE.NOT_NULL)
	private String username;

	@NotNull(message = ERROR_MESSAGE.NOT_NULL)
	private String password;

}
