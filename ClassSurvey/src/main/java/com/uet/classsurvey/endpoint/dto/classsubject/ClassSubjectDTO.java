package com.uet.classsurvey.endpoint.dto.classsubject;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ClassSubjectDTO {
	private String name;
	private String lecturerCode;
	private String subjectName;
	private String time;
	private String schedule;
	private int semester;
	private int credits;
	private String classroom;
	private List<String> listStudentCode;

}
