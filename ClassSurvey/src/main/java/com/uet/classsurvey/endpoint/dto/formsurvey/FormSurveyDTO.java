package com.uet.classsurvey.endpoint.dto.formsurvey;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FormSurveyDTO {

	private int id;
	private String code;
	private String name;
	private String fieldData;
	private int isDefault;

}
