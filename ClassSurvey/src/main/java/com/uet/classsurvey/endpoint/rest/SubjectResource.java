package com.uet.classsurvey.endpoint.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.uet.classsurvey.common.util.JsonHelper;
import com.uet.classsurvey.common.util.Constants.RESPONSE_CODE;
import com.uet.classsurvey.common.util.Constants.RESPONSE_MESSAGE;
import com.uet.classsurvey.dao.entities.Subject;
import com.uet.classsurvey.endpoint.dto.common.ResponseData;
import com.uet.classsurvey.exception.Result;
import com.uet.classsurvey.service.SubjectService;

@Controller
@RequestMapping("/api/uet/class-survey")
public class SubjectResource {

	@Autowired
	SubjectService subjectService;

	@PostMapping(value = "/subjects", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<ResponseData<Integer>> create(@Valid @RequestBody Subject subject) {
		ResponseData<Integer> response = new ResponseData<>();
		Result result = subjectService.create(subject);
		response.setCode(result.getCode());
		response.setMessage(result.getMessage());
		if (result.isSuccess()) {
			return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.CREATED);
		}
		return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.BAD_REQUEST);
	}

	@PutMapping(value = "/subjects", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<ResponseData<Integer>> updateSubjects(@Valid @RequestBody Subject subject) {
		ResponseData<Integer> response = new ResponseData<>();
		Result result = subjectService.update(subject);
		response.setCode(result.getCode());
		response.setMessage(result.getMessage());
		if (result.isSuccess()) {
			return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.OK);
		}
		return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.BAD_REQUEST);
	}

	@DeleteMapping(value = "/subjects/{id}")
	public ResponseEntity<ResponseData<Integer>> deleteSubjectById(@PathVariable("id") int id) {
		ResponseData<Integer> response = new ResponseData<>();
		Result result = subjectService.delete(id);
		response.setCode(result.getCode());
		response.setMessage(result.getMessage());
		if (result.isSuccess()) {
			return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.OK);
		}
		return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.BAD_REQUEST);
	}

	@GetMapping(value = "/subjects/{id}")
	public ResponseEntity<ResponseData<Subject>> findOneSubjects(@PathVariable("id") int id) {
		ResponseData<Subject> response = new ResponseData<>();
		Subject subject = subjectService.findOne(id);
		if (subject != null) {
			response.setCode(RESPONSE_CODE.SUCCESS_CODE);
			response.setMessage(RESPONSE_MESSAGE.SUCCESS_MSG);
			response.setData(subject);
			return new ResponseEntity<ResponseData<Subject>>(response, HttpStatus.OK);
		}
		response.setCode(RESPONSE_CODE.ERR_CODE_BAD_REQUEST);
		response.setMessage(RESPONSE_MESSAGE.ERR_MSG_BAD_REQUEST);
		response.setData(subject);
		return new ResponseEntity<ResponseData<Subject>>(response, HttpStatus.BAD_REQUEST);
	}

	@GetMapping(value = "/list/subjects", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<ResponseData<Object>> getAll() {
		ResponseData<Object> response = new ResponseData<>();
		List<Subject> listSubjects = subjectService.getList();
		if (listSubjects != null) {
			response.setCode(RESPONSE_CODE.SUCCESS_CODE);
			response.setMessage(RESPONSE_MESSAGE.SUCCESS_MSG);
			response.setData(listSubjects);
			return new ResponseEntity<ResponseData<Object>>(response, HttpStatus.OK);
		}
		response.setCode(RESPONSE_CODE.ERR_CODE_BAD_REQUEST);
		response.setMessage(RESPONSE_MESSAGE.ERR_MSG_BAD_REQUEST);
		response.setData(listSubjects);
		return new ResponseEntity<ResponseData<Object>>(response, HttpStatus.BAD_REQUEST);

	}

	@GetMapping(value = "/subjects", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<ResponseData<Object>> getPage(@RequestParam("search") String searchJson, Pageable pageable) {
		ResponseData<Object> response = new ResponseData<>();
		Subject subject = JsonHelper.convertToObject(searchJson, Subject.class);

		Page<Subject> pageSubjects = subjectService.advanceSearch(subject, pageable);
		if (pageSubjects != null) {
			response.setCode(RESPONSE_CODE.SUCCESS_CODE);
			response.setMessage(RESPONSE_MESSAGE.SUCCESS_MSG);
			response.setData(pageSubjects);
			return new ResponseEntity<ResponseData<Object>>(response, HttpStatus.OK);
		}
		response.setCode(RESPONSE_CODE.ERR_CODE_BAD_REQUEST);
		response.setMessage(RESPONSE_MESSAGE.ERR_MSG_BAD_REQUEST);
		response.setData(pageSubjects);
		return new ResponseEntity<ResponseData<Object>>(response, HttpStatus.BAD_REQUEST);

	}

}
