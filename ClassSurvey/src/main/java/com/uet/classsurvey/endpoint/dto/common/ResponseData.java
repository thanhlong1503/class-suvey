package com.uet.classsurvey.endpoint.dto.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.uet.classsurvey.exception.Result;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Response data form
 * 
 * @author LongTT
 *
 * @param <T>
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ResponseData<T> {

	/** the response code */
	private int code;

	/** the response message */
	private String message;

	/** the response data */
	private T data;

	/** the contructor */
	public ResponseData(Result result) {
		this.code = result.getCode();
		this.message = result.getMessage();
	}

	/** the contructor */
	public ResponseData(Result result, T data) {
		this.code = result.getCode();
		this.message = result.getMessage();
		this.data = data;
	}

	@JsonIgnore
	public boolean isSuccess() {
		return (this.code == 1);
	}

}