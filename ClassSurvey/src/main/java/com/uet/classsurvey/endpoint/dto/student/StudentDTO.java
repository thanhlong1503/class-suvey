package com.uet.classsurvey.endpoint.dto.student;

import javax.validation.constraints.NotNull;

import com.uet.classsurvey.common.util.Constants.ERROR_MESSAGE;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StudentDTO {

	private int id;
	@NotNull(message = ERROR_MESSAGE.NOT_NULL)
	private String studentCode;
	private String password;
	@NotNull(message = ERROR_MESSAGE.NOT_NULL)
	private String fullName;
	private String email;
	private String course;

}
