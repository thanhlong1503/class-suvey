package com.uet.classsurvey.endpoint.rest;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uet.classsurvey.authentication.core.TokenState;
import com.uet.classsurvey.authentication.core.UserData;
import com.uet.classsurvey.authentication.helper.TokenHelper;
import com.uet.classsurvey.authentication.service.UsernamePasswordAuthenService;
import com.uet.classsurvey.common.util.Constants;
import com.uet.classsurvey.common.util.Constants.RESPONSE_CODE;
import com.uet.classsurvey.common.util.Constants.RESPONSE_MESSAGE;
import com.uet.classsurvey.endpoint.dto.auth.ChangePasswordDTO;
import com.uet.classsurvey.endpoint.dto.auth.LoginDTO;
import com.uet.classsurvey.endpoint.dto.common.ResponseData;
import com.uet.classsurvey.exception.Result;
import com.uet.classsurvey.service.UserService;

@Controller
@RequestMapping("/api/uet/class-survey")
public class AuthResource {

	@Autowired
	private UserService userService;

	@Autowired
	private UsernamePasswordAuthenService usernamePasswordAuthenService;

	@Autowired
	private TokenHelper tokenHelper;

	@PostMapping(value = "/auth/generate-admin", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<ResponseData<Integer>> get() {
		Result result = userService.generateAdmin();
		ResponseData<Integer> response = new ResponseData<Integer>();
		response.setCode(result.getCode());
		response.setMessage(result.getMessage());
		if (result.isSuccess()) {
			return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.CREATED);
		}
		return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.BAD_REQUEST);
	}

	@PostMapping(value = "/auth/login", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<ResponseData<UserData>> login(@Valid @RequestBody LoginDTO loginRequest) {
		ResponseData<UserData> response = new ResponseData<UserData>();
		UserData userData = usernamePasswordAuthenService.login(loginRequest);
		if (userData != null) {
			response.setCode(Result.SUCCESS.getCode());
			response.setMessage(Result.SUCCESS.getMessage());
			response.setData(userData);
			return new ResponseEntity<ResponseData<UserData>>(response, HttpStatus.OK);
		} else {
			response.setCode(Result.VALID_USER_FAIL.getCode());
			response.setMessage(Result.VALID_USER_FAIL.getMessage());
			response.setData(null);
		}

		return new ResponseEntity<ResponseData<UserData>>(response, HttpStatus.UNAUTHORIZED);
	}

	@PostMapping(value = "/auth/token")
	public ResponseEntity<ResponseData<UserData>> getUserByToken(@RequestBody String token) {
		ResponseData<UserData> response = new ResponseData<UserData>();
		if (token != null) {
			String username = tokenHelper.getUsernameFromToken(token);
			if (username.isEmpty()) {
				response.setCode(RESPONSE_CODE.ERR_CODE_UNAUTHORIZED);
				response.setMessage(RESPONSE_MESSAGE.ERR_MSG_UNAUTHORIZED);
			} else {
				TokenState tokenState = tokenHelper.getTokenExpire(token);
				UserData userData = new UserData();
				userData.setTokenState(tokenState);
				response.setCode(RESPONSE_CODE.SUCCESS_CODE);
				response.setMessage(RESPONSE_MESSAGE.SUCCESS_MSG);
				response.setData(userData);
				return new ResponseEntity<ResponseData<UserData>>(response, HttpStatus.OK);
			}
		}
		return new ResponseEntity<ResponseData<UserData>>(response, HttpStatus.UNAUTHORIZED);
	}

	@PutMapping(value = "/auth/change-password", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<ResponseData<Integer>> changePassWord(
			@Valid @RequestBody ChangePasswordDTO changePasswordRequest) {
		ResponseData<Integer> response = new ResponseData<>();
		changePasswordRequest.setUserName(Constants.getCurrentUser());
		Result result = userService.changePassword(changePasswordRequest);
		response.setCode(result.getCode());
		response.setMessage(result.getMessage());
		if (result.isSuccess()) {
			return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.OK);
		}
		return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.BAD_REQUEST);
	}

}
