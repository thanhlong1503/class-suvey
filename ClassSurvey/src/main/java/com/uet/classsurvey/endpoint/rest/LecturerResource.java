package com.uet.classsurvey.endpoint.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.validation.Valid;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.uet.classsurvey.common.util.JsonHelper;
import com.uet.classsurvey.common.util.Constants.RESPONSE_CODE;
import com.uet.classsurvey.common.util.Constants.RESPONSE_MESSAGE;
import com.uet.classsurvey.dao.entities.Lecturer;
import com.uet.classsurvey.endpoint.dto.common.ResponseData;
import com.uet.classsurvey.endpoint.dto.lecturer.LecturerDTO;
import com.uet.classsurvey.exception.Result;
import com.uet.classsurvey.service.LecturerService;

@Controller
@RequestMapping("/api/uet/class-survey")
public class LecturerResource {

	@Autowired
	LecturerService lecturerService;

	@PostMapping(value = "/lecturers", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<ResponseData<Integer>> create(@Valid @RequestBody LecturerDTO lecturer) {
		ResponseData<Integer> response = new ResponseData<>();
		Result result = lecturerService.create(lecturer);
		response.setCode(result.getCode());
		response.setMessage(result.getMessage());
		if (result.isSuccess()) {
			return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.CREATED);
		}
		return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.BAD_REQUEST);
	}

	@PutMapping(value = "/lecturers", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<ResponseData<Integer>> updateLecturers(@Valid @RequestBody LecturerDTO lecturer) {
		ResponseData<Integer> response = new ResponseData<>();
		Result result = lecturerService.update(lecturer);
		response.setCode(result.getCode());
		response.setMessage(result.getMessage());
		if (result.isSuccess()) {
			return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.OK);
		}
		return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.BAD_REQUEST);
	}

	@DeleteMapping(value = "/lecturers/{id}")
	public ResponseEntity<ResponseData<Integer>> deleteLecturerById(@PathVariable("id") int id) {
		ResponseData<Integer> response = new ResponseData<>();
		Result result = lecturerService.delete(id);
		response.setCode(result.getCode());
		response.setMessage(result.getMessage());
		if (result.isSuccess()) {
			return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.OK);
		}
		return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.BAD_REQUEST);
	}

	@GetMapping(value = "/lecturers/{id}")
	public ResponseEntity<ResponseData<Lecturer>> findOneLecturers(@PathVariable("id") int id) {
		ResponseData<Lecturer> response = new ResponseData<>();
		Lecturer lecturer = lecturerService.findOne(id);
		if (lecturer != null) {
			response.setCode(RESPONSE_CODE.SUCCESS_CODE);
			response.setMessage(RESPONSE_MESSAGE.SUCCESS_MSG);
			response.setData(lecturer);
			return new ResponseEntity<ResponseData<Lecturer>>(response, HttpStatus.OK);
		}
		response.setCode(RESPONSE_CODE.ERR_CODE_BAD_REQUEST);
		response.setMessage(RESPONSE_MESSAGE.ERR_MSG_BAD_REQUEST);
		response.setData(lecturer);
		return new ResponseEntity<ResponseData<Lecturer>>(response, HttpStatus.BAD_REQUEST);
	}

	@GetMapping(value = "/list/lecturers", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<ResponseData<Object>> getAll() {
		ResponseData<Object> response = new ResponseData<>();
		List<Lecturer> listLecturers = lecturerService.getList();
		if (listLecturers != null) {
			response.setCode(RESPONSE_CODE.SUCCESS_CODE);
			response.setMessage(RESPONSE_MESSAGE.SUCCESS_MSG);
			response.setData(listLecturers);
			return new ResponseEntity<ResponseData<Object>>(response, HttpStatus.OK);
		}
		response.setCode(RESPONSE_CODE.ERR_CODE_BAD_REQUEST);
		response.setMessage(RESPONSE_MESSAGE.ERR_MSG_BAD_REQUEST);
		response.setData(listLecturers);
		return new ResponseEntity<ResponseData<Object>>(response, HttpStatus.BAD_REQUEST);

	}

	@GetMapping(value = "/lecturers", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<ResponseData<Object>> getPage(@RequestParam("search") String searchJson, Pageable pageable) {
		ResponseData<Object> response = new ResponseData<>();
		Lecturer Lecturer = JsonHelper.convertToObject(searchJson, Lecturer.class);

		Page<Lecturer> pageLecturers = lecturerService.advanceSearch(Lecturer, pageable);
		if (pageLecturers != null) {
			response.setCode(RESPONSE_CODE.SUCCESS_CODE);
			response.setMessage(RESPONSE_MESSAGE.SUCCESS_MSG);
			response.setData(pageLecturers);
			return new ResponseEntity<ResponseData<Object>>(response, HttpStatus.OK);
		}
		response.setCode(RESPONSE_CODE.ERR_CODE_BAD_REQUEST);
		response.setMessage(RESPONSE_MESSAGE.ERR_MSG_BAD_REQUEST);
		response.setData(pageLecturers);
		return new ResponseEntity<ResponseData<Object>>(response, HttpStatus.BAD_REQUEST);

	}

	@PostMapping("lecturers/uploadFile")
	public ResponseEntity<ResponseData<Integer>> uploadFile(@RequestParam("file") MultipartFile file) {
		ResponseData<Integer> response = new ResponseData<>();
		List<LecturerDTO> listLecturers = new ArrayList<>();

		try {
			XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
			XSSFSheet sheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = sheet.iterator();
			Row currentRow = iterator.next();
			while (iterator.hasNext()) {
				LecturerDTO lecturerDTO = new LecturerDTO();

				currentRow = iterator.next();
				Iterator<Cell> cellIterator = currentRow.iterator();

				Cell currentCell = cellIterator.next();

				if (!cellIterator.hasNext()) {
					break;
				}

				// get username
				currentCell = cellIterator.next();
				lecturerDTO.setUsername(this.getValue(currentCell));

				// get lecturer code
				currentCell = cellIterator.next();
				lecturerDTO.setLecturerCode(this.getValue(currentCell));

				// get password
				currentCell = cellIterator.next();
				lecturerDTO.setPassword(this.getValue(currentCell));

				// get fullname
				currentCell = cellIterator.next();
				lecturerDTO.setFullName(this.getValue(currentCell));

				// get email
				currentCell = cellIterator.next();
				lecturerDTO.setEmail(this.getValue(currentCell));

				if (lecturerDTO.getLecturerCode() == null || lecturerDTO.getLecturerCode().isEmpty()) {
					break;
				}

				listLecturers.add(lecturerDTO);
			}
			// save
			Result result = lecturerService.create(listLecturers);
			response.setCode(result.getCode());
			response.setMessage(result.getMessage());

			if (result.isSuccess()) {
				return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.CREATED);
			}
			return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.BAD_REQUEST);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ResponseEntity<ResponseData<Integer>>(response, HttpStatus.BAD_REQUEST);

	}

	private String getValue(Cell cell) {
		String str = "";
		try {
			if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING) {
				str = cell.getStringCellValue();
			} else if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC) {
				str = (int) cell.getNumericCellValue() + "";
			} else {
				str = cell.getStringCellValue();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return str;
	}

}
