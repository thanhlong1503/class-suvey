package com.uet.classsurvey.endpoint.test;

import org.springframework.web.bind.annotation.RestController;

import com.uet.classsurvey.dao.entities.User;
import com.uet.classsurvey.service.UserService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class TestController {

	@Autowired
	UserService userService;

	@RequestMapping("/")
	public String index() {
		return "Greetings from Spring Boot!";
	}

	@RequestMapping("/users")
	public List<User> getListUser() {
		return userService.getList();
	}

}