package com.uet.classsurvey.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.uet.classsurvey.dao.entities.ClassSubject;
import com.uet.classsurvey.endpoint.dto.classsubject.ClassSubjectDTO;
import com.uet.classsurvey.exception.Result;

public interface ClassSubjectService {

	Result create(ClassSubjectDTO classSubjectDTO);

	Result create(ClassSubject classSubject);

	Result update(ClassSubject classSubject);

	Result delete(int id);

	ClassSubject findOne(int id);

	List<ClassSubject> getList();

	Page<ClassSubject> getPage(Pageable pageable);

	Page<ClassSubject> advanceSearch(ClassSubject classSubject, Pageable pageable);

}
