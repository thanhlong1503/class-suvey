package com.uet.classsurvey.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.uet.classsurvey.dao.entities.User;
import com.uet.classsurvey.endpoint.dto.auth.ChangePasswordDTO;
import com.uet.classsurvey.endpoint.dto.common.ResponseData;
import com.uet.classsurvey.exception.Result;


public interface UserService {
	
	Result generateAdmin();
	
	Result create(User user);

	Result update(User user);
	
	ResponseData<String> resetPassword(int id);

	Result delete(int[] ids);
	
	User findOne(int id);

	List<User> getList();
	
	Page<User> getPage(Pageable pageable);
	
	Result changePassword(ChangePasswordDTO changePasswordRequest);

	Page<User> advanceSearch(User user, Pageable pageable);

}
