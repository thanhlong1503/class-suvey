package com.uet.classsurvey.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.uet.classsurvey.common.util.Constants;
import com.uet.classsurvey.dao.entities.ClassSubject;
import com.uet.classsurvey.dao.entities.Lecturer;
import com.uet.classsurvey.dao.entities.Student;
import com.uet.classsurvey.dao.entities.StudentClassSubject;
import com.uet.classsurvey.dao.entities.Subject;
import com.uet.classsurvey.dao.repository.ClassSubjectRepository;
import com.uet.classsurvey.dao.repository.LecturerRepository;
import com.uet.classsurvey.dao.repository.StudentClassSubjectRepository;
import com.uet.classsurvey.dao.repository.StudentRepository;
import com.uet.classsurvey.dao.repository.SubjectRepository;
import com.uet.classsurvey.dao.specification.ClassSubjectSpecification;
import com.uet.classsurvey.endpoint.dto.classsubject.ClassSubjectDTO;
import com.uet.classsurvey.exception.Result;
import com.uet.classsurvey.service.ClassSubjectService;

@Service
public class ClassSubjectServiceImpl implements ClassSubjectService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ClassSubjectServiceImpl.class);

	@Autowired
	ClassSubjectRepository classSubjectRepository;

	@Autowired
	LecturerRepository lecturerRepository;

	@Autowired
	StudentRepository studentRepository;

	@Autowired
	SubjectRepository subjectRepository;

	@Autowired
	StudentClassSubjectRepository studentClassSubjectRepository;

	@Transactional
	@Override
	public Result create(ClassSubjectDTO classSubjectDTO) {
		try {
			// get lecturer
			Lecturer lecturer = lecturerRepository.findByLecturerCode(classSubjectDTO.getLecturerCode());
			// get subject
			String subjectCode = classSubjectDTO.getName().substring(0, 7);
			Subject subject = subjectRepository.findByCode(classSubjectDTO.getName().substring(0, 7));

			ClassSubject classSubject = new ClassSubject();
			classSubject.setName(classSubjectDTO.getName());
			classSubject.setLecturer(lecturer);
			classSubject.setSubject(subject);

			classSubject.setClassroom(classSubjectDTO.getClassroom());
			classSubject.setSchedule(classSubjectDTO.getSchedule());

			classSubject.setCreated(Constants.getNowTime());
			// set date modified
			classSubject.setModified(Constants.getNowTime());
			// set modifier
			classSubject.setCreator(Constants.getCurrentUser());
			// set modifier
			classSubject.setModifier(Constants.getCurrentUser());

			classSubjectRepository.save(classSubject);

			List<StudentClassSubject> studentClassSubjects = new ArrayList<StudentClassSubject>();
			for (String studentCode : classSubjectDTO.getListStudentCode()) {
				StudentClassSubject studentClassSubject = new StudentClassSubject();
				Student student = studentRepository.findByStudentCode(studentCode.trim());

				studentClassSubject.setClassSubject(classSubject);
				studentClassSubject.setStudent(student);

				studentClassSubject.setCreated(Constants.getNowTime());
				// set date modified
				studentClassSubject.setModified(Constants.getNowTime());
				// set modifier
				studentClassSubject.setCreator(Constants.getCurrentUser());
				// set modifier
				studentClassSubject.setModifier(Constants.getCurrentUser());

				studentClassSubjects.add(studentClassSubject);
			}

			studentClassSubjectRepository.saveAll(studentClassSubjects);

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return Result.BAD_REQUEST;
		}
		// TODO Auto-generated method stub
		return Result.SUCCESS;
	}

	@Transactional
	@Override
	public Result create(ClassSubject classSubject) {
		try {
			ClassSubject ClassSubjectExisting = classSubjectRepository.findById(classSubject.getId());
			if (ClassSubjectExisting == null) {

				classSubject.setCreated(Constants.getNowTime());
				// set date modified
				classSubject.setModified(Constants.getNowTime());
				// set modifier
				classSubject.setCreator(Constants.getCurrentUser());
				// set modifier
				classSubject.setModifier(Constants.getCurrentUser());

				classSubjectRepository.save(classSubject);

			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return Result.BAD_REQUEST;
		}
		// TODO Auto-generated method stub
		return Result.SUCCESS;
	}

	@Override
	public Result update(ClassSubject classSubject) {
		try {
			ClassSubject classSubjectExisting = classSubjectRepository.findById(classSubject.getId());
			if (classSubjectExisting != null) {
				// set date modified
				classSubject.setCreated(classSubjectExisting.getCreated());
				// set date modified
				classSubject.setModified(Constants.getNowTime());
				// set modifier
				classSubject.setCreator(classSubjectExisting.getCreator());
				// set modifier
				classSubject.setModifier(Constants.getCurrentUser());

				classSubjectRepository.save(classSubject);

			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return Result.BAD_REQUEST;
		}
		// TODO Auto-generated method stub
		return Result.SUCCESS;
	}

	@Override
	public Result delete(int id) {
		try {
			classSubjectRepository.deleteById(id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return Result.BAD_REQUEST;
		}
		return Result.SUCCESS;
	}

	@Override
	public ClassSubject findOne(int id) {
		ClassSubject classSubject = null;
		try {
			classSubject = classSubjectRepository.findById(id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return classSubject;
	}

	@Override
	public List<ClassSubject> getList() {
		List<ClassSubject> liClassSubjects = null;
		try {
			liClassSubjects = classSubjectRepository.findAll();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return liClassSubjects;
	}

	@Override
	public Page<ClassSubject> getPage(Pageable pageable) {
		Page<ClassSubject> pageClassSubject = null;
		try {
			pageClassSubject = classSubjectRepository.findAll(pageable);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return pageClassSubject;
	}

	@Override
	public Page<ClassSubject> advanceSearch(ClassSubject classSubject, Pageable pageable) {
		Page<ClassSubject> pageClassSubject = null;
		try {
			pageClassSubject = classSubjectRepository.findAll(ClassSubjectSpecification.advanceSearch(classSubject),
					pageable);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return pageClassSubject;
	}

}
