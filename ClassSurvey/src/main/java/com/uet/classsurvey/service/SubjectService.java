package com.uet.classsurvey.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.uet.classsurvey.dao.entities.Subject;
import com.uet.classsurvey.exception.Result;

public interface SubjectService {

	Result create(Subject subject);

	Result update(Subject subject);

	Result delete(int id);

	Result create(List<Subject> subjects);

	Subject findOne(int id);

	List<Subject> getList();

	Page<Subject> getPage(Pageable pageable);

	Page<Subject> advanceSearch(Subject subject, Pageable pageable);

}
