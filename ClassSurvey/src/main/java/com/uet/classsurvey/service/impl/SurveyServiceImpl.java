package com.uet.classsurvey.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.uet.classsurvey.common.util.Constants;
import com.uet.classsurvey.dao.entities.FormSurvey;
import com.uet.classsurvey.dao.entities.Student;
import com.uet.classsurvey.dao.entities.StudentClassSubject;
import com.uet.classsurvey.dao.entities.Survey;
import com.uet.classsurvey.dao.repository.FormSurveyRepository;
import com.uet.classsurvey.dao.repository.StudentClassSubjectRepository;
import com.uet.classsurvey.dao.repository.StudentRepository;
import com.uet.classsurvey.dao.repository.SurveyRepository;
import com.uet.classsurvey.exception.Result;
import com.uet.classsurvey.service.SurveyService;

@Service
public class SurveyServiceImpl implements SurveyService {

	private static final Logger LOGGER = LoggerFactory.getLogger(SurveyServiceImpl.class);

	@Autowired
	private SurveyRepository surveyRepository;

	@Autowired
	private FormSurveyRepository formSurveyRepository;

	@Autowired
	private StudentClassSubjectRepository studentClassSubjectRepository;

	@Autowired
	private StudentRepository studentRepository;

	@Override
	@Transactional
	public Result generateSurvey() {
		try {
			// get form default
			List<FormSurvey> liFormSurveys = formSurveyRepository.findByIsDefault(1);
			if (liFormSurveys != null && liFormSurveys.size() > 0) {
				FormSurvey formDefault = liFormSurveys.get(0);
				List<StudentClassSubject> liStudentClassSubjects = studentClassSubjectRepository.findAll();
				List<Survey> liSurveys = new ArrayList<>();

				if (liStudentClassSubjects != null) {
					for (StudentClassSubject studentClassSubject : liStudentClassSubjects) {
						if (studentClassSubject.getSurvey() == null) {
							// create new survey
							Survey survey = new Survey();
							survey.setFormSurvey(formDefault);
							survey.setStudentClassSubject(studentClassSubject);
							survey.setCreator(Constants.getCurrentUser());
							survey.setCreated(Constants.getNowTime());
							// save survey
							liSurveys.add(survey);
						}
					}

					// save survey
					surveyRepository.saveAll(liSurveys);
					return Result.SUCCESS;
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return Result.BAD_REQUEST;
	}

	@Override
	public Result updateSurvey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<Survey> getPage(Pageable pageable) {

		Page<Survey> pageSurvey = null;
		try {
			pageSurvey = surveyRepository.findAll(pageable);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return pageSurvey;
	}

	@Override
	@Transactional
	public Page<Survey> getPageOfStudent(Pageable pageable) {
		Page<Survey> pageSurvey = null;

		try {
			Student student = studentRepository.findByStudentCode(Constants.getCurrentUser());
			List<StudentClassSubject> liStudentClassSubjects = studentClassSubjectRepository.findByStudent(student);
			List<Survey> liSurveys = new ArrayList<>();
			if (liStudentClassSubjects != null) {
				for (StudentClassSubject studentClassSubject : liStudentClassSubjects) {
					if (studentClassSubject.getSurvey() != null) {
						liSurveys.add(studentClassSubject.getSurvey());
					}
				}

				int start = (int) pageable.getOffset();
				int end = (start + pageable.getPageSize()) > liSurveys.size() ? liSurveys.size()
						: (start + pageable.getPageSize());
				pageSurvey = new PageImpl<>(liSurveys.subList(start, end), pageable, liSurveys.size());
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}

		return pageSurvey;
	}

	@Override
	public Page<Survey> advanceSearch(Pageable pageable, Survey survey) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Survey findOne(int surveyId) {
		Survey survey = null;
		try {
			survey = surveyRepository.findById(surveyId);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return survey;
	}

}
