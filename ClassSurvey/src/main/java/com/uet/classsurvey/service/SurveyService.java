package com.uet.classsurvey.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.uet.classsurvey.dao.entities.Survey;
import com.uet.classsurvey.exception.Result;

public interface SurveyService {

	Result generateSurvey();

	Result updateSurvey();

	Page<Survey> getPage(Pageable pageable);

	Page<Survey> getPageOfStudent(Pageable pageable);

	Page<Survey> advanceSearch(Pageable pageable, Survey survey);

	Survey findOne(int surveyId);

}
