package com.uet.classsurvey.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.uet.classsurvey.common.util.Constants;
import com.uet.classsurvey.dao.entities.Subject;
import com.uet.classsurvey.dao.repository.SubjectRepository;
import com.uet.classsurvey.dao.specification.SubjectSpecification;
import com.uet.classsurvey.exception.Result;
import com.uet.classsurvey.service.SubjectService;

@Service
public class SubjectServiceImpl implements SubjectService {

	private static final Logger LOGGER = LoggerFactory.getLogger(SubjectServiceImpl.class);

	@Autowired
	SubjectRepository SubjectRepository;

	@Transactional
	@Override
	public Result create(Subject Subject) {
		try {
			Subject SubjectExisting = SubjectRepository.findById(Subject.getId());
			if (SubjectExisting == null) {

				// create Subject
				Subject.setCreated(Constants.getNowTime());
				// set date modified
				Subject.setModified(Constants.getNowTime());
				// set modifier
				Subject.setCreator(Constants.getCurrentUser());
				// set modifier
				Subject.setModifier(Constants.getCurrentUser());

				SubjectRepository.save(Subject);

			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return Result.BAD_REQUEST;
		}
		// TODO Auto-generated method stub
		return Result.SUCCESS;
	}

	@Override
	@Transactional
	public Result create(List<Subject> Subjects) {
		try {
			for (Subject Subject : Subjects) {
				// set date created
				Subject.setCreated(Constants.getNowTime());
				// set date modified
				Subject.setModified(Constants.getNowTime());
				// set modifier
				Subject.setCreator(Constants.getCurrentUser());
				// set modifier
				Subject.setModifier(Constants.getCurrentUser());

				// save Subject
				SubjectRepository.save(Subject);

			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return Result.BAD_REQUEST;
		}
		// TODO Auto-generated method stub
		return Result.SUCCESS;
	}

	@Override
	public Result update(Subject Subject) {
		try {
			Subject SubjectExisting = SubjectRepository.findById(Subject.getId());
			if (SubjectExisting != null) {

				// set date modified
				Subject.setCreated(SubjectExisting.getCreated());
				// set date modified
				Subject.setModified(Constants.getNowTime());
				// set modifier
				Subject.setCreator(SubjectExisting.getCreator());
				// set modifier
				Subject.setModifier(Constants.getCurrentUser());

				SubjectRepository.save(Subject);

			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return Result.BAD_REQUEST;
		}
		// TODO Auto-generated method stub
		return Result.SUCCESS;
	}

	@Override
	public Result delete(int id) {
		try {
			SubjectRepository.deleteById(id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return Result.BAD_REQUEST;
		}
		return Result.SUCCESS;
	}

	@Override
	public Subject findOne(int id) {
		Subject Subject = null;
		try {
			Subject = SubjectRepository.findById(id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return Subject;
	}

	@Override
	public List<Subject> getList() {
		List<Subject> liSubjects = null;
		try {
			liSubjects = SubjectRepository.findAll();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return liSubjects;
	}

	@Override
	public Page<Subject> getPage(Pageable pageable) {
		Page<Subject> pageSubject = null;
		try {
			pageSubject = SubjectRepository.findAll(pageable);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return pageSubject;
	}

	@Override
	public Page<Subject> advanceSearch(Subject Subject, Pageable pageable) {
		Page<Subject> pageSubject = null;
		try {
			pageSubject = SubjectRepository.findAll(SubjectSpecification.advanceSearch(Subject), pageable);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return pageSubject;
	}

}
