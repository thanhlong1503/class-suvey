package com.uet.classsurvey.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.uet.classsurvey.common.util.Constants;
import com.uet.classsurvey.dao.entities.FormSurvey;
import com.uet.classsurvey.dao.repository.RoleRepository;
import com.uet.classsurvey.dao.repository.FormSurveyRepository;
import com.uet.classsurvey.dao.repository.UserRepository;
import com.uet.classsurvey.endpoint.dto.formsurvey.FormSurveyDTO;
import com.uet.classsurvey.exception.Result;
import com.uet.classsurvey.service.FormSurveyService;

@Service
public class FormSurveyServiceImpl implements FormSurveyService {

	private static final Logger LOGGER = LoggerFactory.getLogger(FormSurveyServiceImpl.class);

	@Autowired
	FormSurveyRepository formSurveyRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Transactional
	@Override
	public Result create(FormSurveyDTO FormSurveyDTO) {
		try {
			FormSurvey formSurveyExisting = formSurveyRepository.findById(FormSurveyDTO.getId());
			if (formSurveyExisting == null) {

				FormSurvey FormSurvey = toFormSurvey(FormSurveyDTO);
				// create FormSurvey
				formSurveyRepository.save(FormSurvey);

			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return Result.BAD_REQUEST;
		}
		// TODO Auto-generated method stub
		return Result.SUCCESS;
	}

	@Override
	public Result update(FormSurveyDTO formSurveyDTO) {
		try {
			FormSurvey FormSurveyExisting = formSurveyRepository.findById(formSurveyDTO.getId());
			if (FormSurveyExisting != null) {
				// create FormSurvey
				FormSurvey formSurvey = toFormSurvey(formSurveyDTO);

				// set date modified
				formSurvey.setCreated(FormSurveyExisting.getCreated());
				// set modifier
				formSurvey.setCreator(FormSurveyExisting.getCreator());

				formSurveyRepository.save(formSurvey);

			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return Result.BAD_REQUEST;
		}
		// TODO Auto-generated method stub
		return Result.SUCCESS;
	}

	@Override
	public Result delete(int id) {
		try {
			formSurveyRepository.deleteById(id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return Result.BAD_REQUEST;
		}
		return Result.SUCCESS;
	}

	@Override
	public FormSurvey findOne(int id) {
		FormSurvey FormSurvey = null;
		try {
			FormSurvey = formSurveyRepository.findById(id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return FormSurvey;
	}

	@Override
	public List<FormSurvey> getList() {
		List<FormSurvey> liFormSurveys = null;
		try {
			liFormSurveys = formSurveyRepository.findAll();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return liFormSurveys;
	}

	@Override
	public Page<FormSurvey> getPage(Pageable pageable) {
		Page<FormSurvey> pageFormSurvey = null;
		try {
			pageFormSurvey = formSurveyRepository.findAll(pageable);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return pageFormSurvey;
	}

	private FormSurvey toFormSurvey(FormSurveyDTO formSurveyDTO) {
		// create FormSurvey
		FormSurvey formSurvey = new FormSurvey();

		formSurvey.setCode(formSurveyDTO.getCode());
		formSurvey.setName(formSurveyDTO.getName());
		formSurvey.setFieldData(formSurveyDTO.getFieldData());
		formSurvey.setIsDefault(formSurveyDTO.getIsDefault());
		formSurvey.setId(formSurveyDTO.getId());
		// set date created
		formSurvey.setCreated(Constants.getNowTime());
		// set date modified
		formSurvey.setModified(Constants.getNowTime());
		// set modifier
		formSurvey.setCreator(Constants.getCurrentUser());
		// set modifier
		formSurvey.setModifier(Constants.getCurrentUser());
		return formSurvey;
	}

}
