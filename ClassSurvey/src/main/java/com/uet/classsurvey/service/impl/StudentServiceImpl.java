package com.uet.classsurvey.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.uet.classsurvey.common.util.Constants;
import com.uet.classsurvey.dao.entities.Student;
import com.uet.classsurvey.dao.entities.User;
import com.uet.classsurvey.dao.repository.RoleRepository;
import com.uet.classsurvey.dao.repository.StudentRepository;
import com.uet.classsurvey.dao.repository.UserRepository;
import com.uet.classsurvey.dao.specification.StudentSpecification;
import com.uet.classsurvey.endpoint.dto.student.StudentDTO;
import com.uet.classsurvey.exception.Result;
import com.uet.classsurvey.service.StudentService;

@Service
public class StudentServiceImpl implements StudentService {

	private static final Logger LOGGER = LoggerFactory.getLogger(StudentServiceImpl.class);

	@Autowired
	StudentRepository studentRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

	@Transactional
	@Override
	public Result create(StudentDTO studentDTO) {
		try {
			Student studentExisting = studentRepository.findByStudentCode(studentDTO.getStudentCode());
			if (studentExisting == null) {
				// create user
				User user = toUser(studentDTO);
				userRepository.save(user);

				Student student = toStudent(studentDTO);
				// create student
				student.setUser(user);
				studentRepository.save(student);

			} else {
				return Result.STUDENT_IS_EXISTED;
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return Result.BAD_REQUEST;
		}
		// TODO Auto-generated method stub
		return Result.SUCCESS;
	}

	@Override
	@Transactional
	public Result create(List<StudentDTO> students) {
		try {
			for (StudentDTO studentDTO : students) {
				// create user
				User user = toUser(studentDTO);

				// save user
				user = userRepository.save(user);

				// create student
				Student student = toStudent(studentDTO);

				student.setUser(user);

				// save student
				studentRepository.save(student);

			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return Result.BAD_REQUEST;
		}
		// TODO Auto-generated method stub
		return Result.SUCCESS;
	}

	@Override
	public Result update(StudentDTO studentDTO) {
		try {
			Student studentExisting = studentRepository.findById(studentDTO.getId());
			if (studentExisting != null) {
				// create student
				Student student = toStudent(studentDTO);

				// set user
				student.setUser(studentExisting.getUser());
				// set date modified
				student.setCreated(studentExisting.getCreated());
				// set modifier
				student.setCreator(studentExisting.getCreator());

				studentRepository.save(student);

			} else {
				return Result.STUDENT_NOT_EXIST;
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return Result.BAD_REQUEST;
		}
		// TODO Auto-generated method stub
		return Result.SUCCESS;
	}

	@Override
	public Result delete(int id) {
		try {
			Student studentExisting = studentRepository.findById(id);
			if (studentExisting != null) {
				// delete student
				studentRepository.deleteById(id);
				// delete account info
				userRepository.deleteById(studentExisting.getUser().getId());

			} else {
				return Result.STUDENT_NOT_EXIST;
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return Result.BAD_REQUEST;
		}
		// TODO Auto-generated method stub
		return Result.SUCCESS;
	}

	@Override
	public Student findOne(int id) {
		Student student = null;
		try {
			student = studentRepository.findById(id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return student;
	}

	@Override
	public List<Student> getList() {
		List<Student> liStudents = null;
		try {
			liStudents = studentRepository.findAll();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return liStudents;
	}

	@Override
	public Page<Student> getPage(Pageable pageable) {
		Page<Student> pageStudent = null;
		try {
			pageStudent = studentRepository.findAll(pageable);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return pageStudent;
	}

	@Override
	public Page<Student> advanceSearch(Student student, Pageable pageable) {
		Page<Student> pageStudent = null;
		try {
			pageStudent = studentRepository.findAll(StudentSpecification.advanceSearch(student), pageable);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return pageStudent;
	}

	private User toUser(StudentDTO studentDTO) {
		User user = new User();
		user.setRole(roleRepository.findById(3));
		user.setUsername(studentDTO.getStudentCode());
		user.setPassword(passwordEncoder.encode(studentDTO.getPassword().trim()));
		user.setEmail(studentDTO.getEmail());
		// set date created
		user.setCreated(Constants.getNowTime());
		// set date modified
		user.setModified(Constants.getNowTime());
		// set modifier
		user.setCreator(Constants.getCurrentUser());
		// set modifier
		user.setModifier(Constants.getCurrentUser());
		return user;

	}

	private Student toStudent(StudentDTO studentDTO) {
		// create student
		Student student = new Student();

		student.setId(studentDTO.getId());
		// set studetn code
		student.setStudentCode(studentDTO.getStudentCode().trim());
		// set fullname
		student.setFullname(studentDTO.getFullName().trim());
		// set email
		student.setEmail(studentDTO.getEmail());
		// set address
		student.setCourse(studentDTO.getCourse());
		// set date created
		student.setCreated(Constants.getNowTime());
		// set date modified
		student.setModified(Constants.getNowTime());
		// set modifier
		student.setCreator(Constants.getCurrentUser());
		// set modifier
		student.setModifier(Constants.getCurrentUser());
		return student;
	}

}
