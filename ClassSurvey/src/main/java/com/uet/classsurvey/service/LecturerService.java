package com.uet.classsurvey.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.uet.classsurvey.dao.entities.Lecturer;
import com.uet.classsurvey.endpoint.dto.lecturer.LecturerDTO;
import com.uet.classsurvey.exception.Result;

public interface LecturerService {

	Result create(LecturerDTO lecturerDTO);

	Result update(LecturerDTO lecturerDTO);

	Result delete(int id);

	Result create(List<LecturerDTO> lecturers);

	Lecturer findOne(int id);

	List<Lecturer> getList();

	Page<Lecturer> getPage(Pageable pageable);

	Page<Lecturer> advanceSearch(Lecturer lecturer, Pageable pageable);

}
