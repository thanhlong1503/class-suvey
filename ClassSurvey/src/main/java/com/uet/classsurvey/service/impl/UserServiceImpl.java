package com.uet.classsurvey.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.uet.classsurvey.common.util.Constants;
import com.uet.classsurvey.dao.entities.Role;
import com.uet.classsurvey.dao.entities.User;
import com.uet.classsurvey.dao.repository.RoleRepository;
import com.uet.classsurvey.dao.repository.UserRepository;
import com.uet.classsurvey.endpoint.dto.auth.ChangePasswordDTO;
import com.uet.classsurvey.endpoint.dto.common.ResponseData;
import com.uet.classsurvey.exception.Result;
import com.uet.classsurvey.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	public static String DEFAUL_PASSWORD = "admin@123";

	public static String USER_ADMIN = "admin";

	private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

	@Override
	public Result generateAdmin() {
		try {
			User user = new User();
			// set role
			Role role = roleRepository.findById(1);
			user.setRole(role);
			// set username
			user.setUsername(USER_ADMIN);
			// encode password
			user.setPassword(passwordEncoder.encode(USER_ADMIN));
			// set date created
			user.setCreated(Constants.getNowTime());
			// set date modified
			user.setModified(Constants.getNowTime());
			// set modifier
			user.setCreator(Constants.getCurrentUser());
			// set modifier
			user.setModifier(Constants.getCurrentUser());
			userRepository.save(user);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return Result.BAD_REQUEST;
		}
		return Result.SUCCESS;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.uet.service.UserService#create(com.uet.dao.model.User)
	 */
	@Override
	public Result create(User user) {
		try {

			// check userId
			User userExisting = userRepository.findById(user.getId());
			if (userExisting == null) {
				// check username
				userExisting = userRepository.findByUsername(user.getUsername());
				if (userExisting == null) {
					// check role
					if (user.getRole() != null) {
						Role role = roleRepository.findById(user.getRole().getId());
						if (role != null) {
							user.setRole(role);
						} else {
							return Result.ROLE_NOT_EXIST;
						}
					} else {
						return Result.ROLE_NOT_EXIST;
					}
					// encode password
					user.setPassword(passwordEncoder.encode(user.getPassword()));
					// set date created
					user.setCreated(Constants.getNowTime());
					// set date modified
					user.setModified(Constants.getNowTime());
					// set modifier
					user.setCreator(Constants.getCurrentUser());
					// set modifier
					user.setModifier(Constants.getCurrentUser());
					userRepository.save(user);
				} else {
					return Result.USERNAME_IS_EXISTED;
				}

			} else {
				return Result.USER_IS_EXISTED;
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return Result.BAD_REQUEST;
		}
		return Result.SUCCESS;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.uet.service.UserService#update(com.uet.dao.model.User)
	 */
	@Override
	public Result update(User user) {
		try {
			User userExisting = userRepository.findById(user.getId());
			if (userExisting != null) {
				// set role
				if (user.getRole() != null) {
					Role role = roleRepository.findById(user.getRole().getId());
					if (role != null) {
						userExisting.setRole(role);
					} else {
						return Result.ROLE_NOT_EXIST;
					}
				} else {
					return Result.ROLE_NOT_EXIST;
				}
				// set date modifier
				userExisting.setModified(Constants.getNowTime());
				// set modifier
				userExisting.setModifier(Constants.getCurrentUser());
				userRepository.save(userExisting);
				return Result.SUCCESS;
			} else {
				return Result.USER_NOT_EXIST;
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return Result.BAD_REQUEST;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.az.mngt.service.UserService#resetPassword()
	 */
	@Override
	public ResponseData<String> resetPassword(int id) {
		try {
			// check userid
			User userExisting = userRepository.findById(id);
			if (userExisting != null) {
				String pwd = this.generatePassword();
				// set new password
				userExisting.setPassword(passwordEncoder.encode(pwd));
				// set date modifier
				userExisting.setModified(Constants.getNowTime());
				// set modifier
				userExisting.setModifier(Constants.getCurrentUser());
				userRepository.save(userExisting);
				return new ResponseData<>(Result.SUCCESS.getCode(), Result.SUCCESS.getMessage(), pwd);
			} else {
				return new ResponseData<>(Result.USER_NOT_EXIST.getCode(), Result.USER_NOT_EXIST.getMessage(), null);
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return new ResponseData<>(Result.BAD_REQUEST.getCode(), Result.BAD_REQUEST.getMessage(), null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.uet.service.UserService#delete(int[])
	 */
	@Override
	public Result delete(int[] ids) {
		try {
			List<User> entities = new ArrayList<>();
			for (int i = 0; i < ids.length; i++) {
				entities.add(userRepository.getOne(Constants.convertIntToInteger(ids[i])));
			}
			userRepository.deleteInBatch(entities);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return Result.BAD_REQUEST;
		}
		return Result.SUCCESS;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.uet.service.UserService#findOne(int)
	 */
	@Override
	public User findOne(int id) {
		User user = null;
		try {
			user = userRepository.findById(id);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return user;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.uet.service.UserService#findAll()
	 */
	@Override
	public List<User> getList() {
		List<User> listUser = null;
		try {
			listUser = userRepository.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return listUser;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.az.mngt.service.UserService#getPage(org.springframework.data.domain.
	 * Pageable)
	 */
	@Override
	public Page<User> getPage(Pageable pageable) {
		Page<User> pageUsers = null;
		try {
			pageUsers = userRepository.findAll(pageable);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return pageUsers;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.az.mngt.service.UserService#changePassword(com.az.mngt.common.dto.
	 * ChangePasswordRequest)
	 */
	@Override
	public Result changePassword(ChangePasswordDTO changePasswordRequest) {
		// check password and confirm password
		if (changePasswordRequest.getNewPassword().equals(changePasswordRequest.getConfirmPassword())) {
			// check valid username and password
			try {
				User user = userRepository.findByUsername(changePasswordRequest.getUserName());
				if (user != null) {
					// check password
					if (passwordEncoder.matches(changePasswordRequest.getPassword(), user.getPassword())) {
						// set new password
						user.setPassword(passwordEncoder.encode(changePasswordRequest.getNewPassword()));
						// set date modifier
						user.setModified(Constants.getNowTime());
						// set modifier
						user.setModifier(Constants.getCurrentUser());
						userRepository.save(user);
						return Result.SUCCESS;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				LOGGER.error(e.getMessage());
			}
			return Result.VALID_USER_FAIL;
		} else {
			return Result.CONFIRM_PASSWORD;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.uet.service.UserService#advanceSearch(com.uet.dao.model.User,
	 * org.springframework.data.domain.Pageable)
	 */
	@Override
	public Page<User> advanceSearch(User user, Pageable pageable) {
		Page<User> pageUser = null;
		try {
			// pageUser = userRepository.findAll(UserSpecification.advanceFilter(user),
			// pageable);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return pageUser;
	}

	/**
	 * generate a random password
	 * 
	 * @return a random password
	 */
	private String generatePassword() {
		try {
			String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
			return RandomStringUtils.random(10, characters);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return DEFAUL_PASSWORD;
	}

}
