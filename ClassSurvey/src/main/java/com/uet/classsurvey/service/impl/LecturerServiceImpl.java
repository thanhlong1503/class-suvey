package com.uet.classsurvey.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.uet.classsurvey.common.util.Constants;
import com.uet.classsurvey.dao.entities.Lecturer;
import com.uet.classsurvey.dao.entities.User;
import com.uet.classsurvey.dao.repository.RoleRepository;
import com.uet.classsurvey.dao.repository.LecturerRepository;
import com.uet.classsurvey.dao.repository.UserRepository;
import com.uet.classsurvey.dao.specification.LecturerSpecification;
import com.uet.classsurvey.endpoint.dto.lecturer.LecturerDTO;
import com.uet.classsurvey.exception.Result;
import com.uet.classsurvey.service.LecturerService;

@Service
public class LecturerServiceImpl implements LecturerService {

	private static final Logger LOGGER = LoggerFactory.getLogger(LecturerServiceImpl.class);

	@Autowired
	LecturerRepository lecturerRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

	@Transactional
	@Override
	public Result create(LecturerDTO lecturerDTO) {
		try {
			Lecturer LecturerExisting = lecturerRepository.findByLecturerCode(lecturerDTO.getLecturerCode());
			if (LecturerExisting == null) {
				// create user
				User user = toUser(lecturerDTO);
				userRepository.save(user);

				// create Lecturer
				Lecturer lecturer = toLecturer(lecturerDTO);
				// set user
				lecturer.setUser(user);
				// save lecturer
				lecturerRepository.save(lecturer);

			} else {
				return Result.LECTURER_IS_EXISTED;
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return Result.BAD_REQUEST;
		}
		// TODO Auto-generated method stub
		return Result.SUCCESS;
	}

	@Override
	@Transactional
	public Result create(List<LecturerDTO> lecturers) {
		try {
			for (LecturerDTO lecturerDTO : lecturers) {

				// create user
				User user = toUser(lecturerDTO);

				// save user
				user = userRepository.save(user);

				// create Lecturer
				Lecturer lecturer = toLecturer(lecturerDTO);
				// set user
				lecturer.setUser(user);

				// save Lecturer
				lecturerRepository.save(lecturer);

			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return Result.BAD_REQUEST;
		}
		// TODO Auto-generated method stub
		return Result.SUCCESS;
	}

	@Override
	public Result update(LecturerDTO lecturerDTO) {
		try {
			Lecturer lecturerExisting = lecturerRepository.findById(lecturerDTO.getId());
			if (lecturerExisting != null) {
				Lecturer lecturer = toLecturer(lecturerDTO);

				// create Lecturer
				lecturer.setUser(lecturerExisting.getUser());
				// set date created
				lecturer.setCreated(lecturerExisting.getCreated());
				// set creator
				lecturer.setCreator(lecturerExisting.getCreator());

				lecturerRepository.save(lecturer);

			} else {
				return Result.LECTURER_NOT_EXIST;
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return Result.BAD_REQUEST;
		}
		// TODO Auto-generated method stub
		return Result.SUCCESS;
	}

	@Override
	@Transactional
	public Result delete(int id) {
		try {
			Lecturer lecturerExisting = lecturerRepository.findById(id);
			if (lecturerExisting != null) {
				// delete lecturer info
				lecturerRepository.deleteById(id);
				// delete account info
				userRepository.deleteById(lecturerExisting.getUser().getId());

			} else {
				return Result.LECTURER_NOT_EXIST;
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return Result.BAD_REQUEST;
		}
		// TODO Auto-generated method stub
		return Result.SUCCESS;
	}

	@Override
	public Lecturer findOne(int id) {
		Lecturer Lecturer = null;
		try {
			Lecturer = lecturerRepository.findById(id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return Lecturer;
	}

	@Override
	public List<Lecturer> getList() {
		List<Lecturer> liLecturers = null;
		try {
			liLecturers = lecturerRepository.findAll();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return liLecturers;
	}

	@Override
	public Page<Lecturer> getPage(Pageable pageable) {
		Page<Lecturer> pageLecturer = null;
		try {
			pageLecturer = lecturerRepository.findAll(pageable);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return pageLecturer;
	}

	@Override
	public Page<Lecturer> advanceSearch(Lecturer lecturer, Pageable pageable) {
		Page<Lecturer> pageLecturer = null;
		try {
			pageLecturer = lecturerRepository.findAll(LecturerSpecification.advanceSearch(lecturer), pageable);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return pageLecturer;
	}

	private Lecturer toLecturer(LecturerDTO lecturerDTO) {
		// create Lecturer
		Lecturer lecturer = new Lecturer();

		lecturer.setId(lecturerDTO.getId());
		// set lecturer code
		lecturer.setLecturerCode(lecturerDTO.getLecturerCode().trim());
		// set full name
		lecturer.setFullname(lecturerDTO.getFullName().trim());
		// set email
		lecturer.setEmail(lecturerDTO.getEmail());

		// set date created
		lecturer.setCreated(Constants.getNowTime());
		// set date modified
		lecturer.setModified(Constants.getNowTime());
		// set modifier
		lecturer.setCreator(Constants.getCurrentUser());
		// set modifier
		lecturer.setModifier(Constants.getCurrentUser());

		return lecturer;
	}

	private User toUser(LecturerDTO lecturerDTO) {
		User user = new User();
		user.setRole(roleRepository.findById(2));
		user.setUsername(lecturerDTO.getUsername().trim());
		user.setPassword(passwordEncoder.encode(lecturerDTO.getPassword().trim()));
		user.setEmail(lecturerDTO.getEmail());
		// set date created
		user.setCreated(Constants.getNowTime());
		// set date modified
		user.setModified(Constants.getNowTime());
		// set modifier
		user.setCreator(Constants.getCurrentUser());
		// set modifier
		user.setModifier(Constants.getCurrentUser());
		return user;
	}

}
