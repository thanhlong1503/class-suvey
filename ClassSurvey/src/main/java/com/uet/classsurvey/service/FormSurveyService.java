package com.uet.classsurvey.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.uet.classsurvey.dao.entities.FormSurvey;
import com.uet.classsurvey.endpoint.dto.formsurvey.FormSurveyDTO;
import com.uet.classsurvey.exception.Result;

public interface FormSurveyService {

	Result create(FormSurveyDTO formSurvey);

	Result update(FormSurveyDTO formSurvey);

	Result delete(int id);

	FormSurvey findOne(int id);

	List<FormSurvey> getList();

	Page<FormSurvey> getPage(Pageable pageable);

}
