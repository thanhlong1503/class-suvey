package com.uet.classsurvey.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.uet.classsurvey.dao.entities.Student;
import com.uet.classsurvey.endpoint.dto.student.StudentDTO;
import com.uet.classsurvey.exception.Result;

public interface StudentService {

	Result create(StudentDTO student);

	Result update(StudentDTO student);

	Result delete(int id);

	Result create(List<StudentDTO> students);

	Student findOne(int id);

	List<Student> getList();

	Page<Student> getPage(Pageable pageable);

	Page<Student> advanceSearch(Student student, Pageable pageable);

}
