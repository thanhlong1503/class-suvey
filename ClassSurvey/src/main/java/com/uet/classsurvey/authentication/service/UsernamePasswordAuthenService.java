package com.uet.classsurvey.authentication.service;

import com.uet.classsurvey.authentication.core.UserData;
import com.uet.classsurvey.endpoint.dto.auth.LoginDTO;

/**
 * 
 * @author LongTT
 *
 */
public interface UsernamePasswordAuthenService {
	
	/**
	 * check username and password is valid
	 * @param user the user info
	 * @return the {@link UserData}
	 */
	UserData login(LoginDTO user);
		
}
