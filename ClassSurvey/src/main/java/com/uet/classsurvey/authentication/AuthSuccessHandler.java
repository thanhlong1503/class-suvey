package com.uet.classsurvey.authentication;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uet.classsurvey.authentication.core.TokenState;
import com.uet.classsurvey.authentication.core.UserData;
import com.uet.classsurvey.authentication.helper.TokenHelper;
import com.uet.classsurvey.common.util.Constants.RESPONSE_CODE;
import com.uet.classsurvey.common.util.Constants.RESPONSE_MESSAGE;
import com.uet.classsurvey.dao.repository.RoleRepository;
import com.uet.classsurvey.dao.repository.UserRepository;
import com.uet.classsurvey.endpoint.dto.common.ResponseData;

//@Component
public class AuthSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

	@Value("${jwt.expires_time}")
	private int EXPIRES_IN;

	@Autowired
	TokenHelper tokenHelper;

	@Autowired
	ObjectMapper objectMapper;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	private final ObjectMapper mapper;

	AuthSuccessHandler(
			@Qualifier("mappingJackson2HttpMessageConverter") MappingJackson2HttpMessageConverter messageConverter) {
		this.mapper = messageConverter.getObjectMapper();
	}

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {

		ResponseData<UserData> responseData = new ResponseData<UserData>();
		if (authentication != null && authentication.getDetails() != null) {
			try {
				clearAuthenticationAttributes(request);
				User user = (User) authentication.getPrincipal();
				String username = user.getUsername();
				TokenState tokenState = tokenHelper.generateToken(username);
				com.uet.classsurvey.dao.entities.User user2 = userRepository.findByUsername(username);
				// for(RoleNavigation roleNavigation: listRoleNavigation) {
				// NavigationResponse navigationResponse = new
				// NavigationResponse(roleNavigation);
				// liNavigationResponses.add(navigationResponse);
				// }

				UserData userData = new UserData();
				userData.setTokenState(tokenState);
				userData.setUsername(username);

				responseData.setCode(RESPONSE_CODE.SUCCESS_CODE);
				responseData.setMessage(RESPONSE_MESSAGE.SUCCESS_MSG);
				responseData.setData(userData);

				PrintWriter writer = response.getWriter();
				mapper.writeValue(writer, responseData);
				writer.flush();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			responseData.setCode(RESPONSE_CODE.ERR_CODE_UNAUTHORIZED);
			responseData.setMessage(RESPONSE_MESSAGE.ERR_MSG_UNAUTHORIZED);
			responseData.setData(null);
		}
	}

}
