package com.uet.classsurvey.authentication.core;

/**
 * Token State
 * 
 * @author LongTT
 *
 */
public class TokenState {
	
	/** the json web token*/
	private String jwt;
	
	/** the expires time of the token*/
    private long expires;

    public TokenState(){
    	
    }
    
    public TokenState(String jwt, long expires){
        this.jwt = jwt;
        this.expires = expires;
    }

    public String getJwt() {
        return jwt;
    }

    public void setJws(String jwt) {
        this.jwt = jwt;
    }

    public long getExpires() {
        return expires;
    }

    public void setExpires(long expire) {
        this.expires = expire;
    }

}
