package com.uet.classsurvey.authentication;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import com.google.gson.Gson;
import com.uet.classsurvey.authentication.core.UserData;
import com.uet.classsurvey.common.util.Constants.RESPONSE_CODE;
import com.uet.classsurvey.common.util.Constants.RESPONSE_MESSAGE;
import com.uet.classsurvey.endpoint.dto.common.ResponseData;

public class CustomAccessDeniedHandler implements AccessDeniedHandler {

	@Override
	public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException exc)
			throws IOException, ServletException {

		ResponseData<UserData> responseData = new ResponseData<>();
		responseData.setCode(RESPONSE_CODE.ERR_CODE_FORBIDDEN);
		responseData.setMessage(RESPONSE_MESSAGE.ERR_MSG_FORBIDDEN);
		responseData.setData(null);
		String jsonResponseData = new Gson().toJson(responseData);
		
		response.setStatus(HttpServletResponse.SC_FORBIDDEN);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		out.print(jsonResponseData);
		out.flush();

	}
}