package com.uet.classsurvey.authentication;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uet.classsurvey.authentication.core.UserData;
import com.uet.classsurvey.common.util.Constants.RESPONSE_CODE;
import com.uet.classsurvey.common.util.Constants.RESPONSE_MESSAGE;
import com.uet.classsurvey.endpoint.dto.common.ResponseData;

//@Component
public class MyLogoutSuccessHandler implements LogoutSuccessHandler {

	private final ObjectMapper mapper;

	MyLogoutSuccessHandler(
			@Qualifier("mappingJackson2HttpMessageConverter") MappingJackson2HttpMessageConverter messageConverter) {
		this.mapper = messageConverter.getObjectMapper();
	}

	@Override
	public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		ResponseData<UserData> responseData = new ResponseData<>();
		if (authentication != null && authentication.getDetails() != null) {
			try {
				responseData.setCode(RESPONSE_CODE.SUCCESS_CODE);
				responseData.setMessage(RESPONSE_MESSAGE.SUCCESS_MSG);
				responseData.setData(null);
				
				PrintWriter writer = response.getWriter();
				mapper.writeValue(writer, responseData);
				writer.flush();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
