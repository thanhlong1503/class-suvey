package com.uet.classsurvey.authentication;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uet.classsurvey.authentication.core.UserData;
import com.uet.classsurvey.common.util.Constants.RESPONSE_CODE;
import com.uet.classsurvey.common.util.Constants.RESPONSE_MESSAGE;
import com.uet.classsurvey.endpoint.dto.common.ResponseData;

//@Component
public class AuthFailureHandler extends SimpleUrlAuthenticationFailureHandler {

	private final ObjectMapper mapper;

	AuthFailureHandler(
			@Qualifier("mappingJackson2HttpMessageConverter") MappingJackson2HttpMessageConverter messageConverter) {
		this.mapper = messageConverter.getObjectMapper();
	}

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {
		// TODO Auto-generated method stub
		ResponseData<UserData> responseData = new ResponseData<>();
		try {
			responseData.setCode(RESPONSE_CODE.ERR_CODE_UNAUTHORIZED);
			responseData.setMessage(RESPONSE_MESSAGE.ERR_MSG_UNAUTHORIZED);
			responseData.setData(null);
			PrintWriter writer = response.getWriter();
			mapper.writeValue(writer, responseData);
			writer.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
