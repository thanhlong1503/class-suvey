package com.uet.classsurvey.authentication;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import com.google.gson.Gson;
import com.uet.classsurvey.authentication.core.UserData;
import com.uet.classsurvey.common.util.Constants.RESPONSE_CODE;
import com.uet.classsurvey.common.util.Constants.RESPONSE_MESSAGE;
import com.uet.classsurvey.endpoint.dto.common.ResponseData;


public final class RestAuthenticationEntryPoint implements AuthenticationEntryPoint {
	
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException {
		ResponseData<UserData> responseData = new ResponseData<>();
		responseData.setCode(RESPONSE_CODE.ERR_CODE_UNAUTHORIZED);
		responseData.setMessage(RESPONSE_MESSAGE.ERR_MSG_UNAUTHORIZED);
		responseData.setData(null);
		String jsonResponseData = new Gson().toJson(responseData);
		
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		out.print(jsonResponseData);
		out.flush();
	}
}
