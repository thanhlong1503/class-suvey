package com.uet.classsurvey.authentication.helper;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.uet.classsurvey.authentication.core.TokenState;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * 
 * @author LongTT
 *
 */

@Component
public class TokenHelper {

	@Value("${jwt.secret_key}")
	private String SECRET_KEY;

	@Value("${jwt.expires_time}")
	private int EXPIRE_TIME;

	private SignatureAlgorithm SIGNATURE_ALGORITHM = SignatureAlgorithm.HS512;

	private static final Logger LOGGER = LoggerFactory.getLogger(TokenHelper.class);

	/**
	 * generate accessToken
	 * 
	 * @param username
	 * @return
	 */
	public TokenState generateToken(String username) {
		String jws = Jwts.builder().setSubject(username).setIssuedAt(generateCurrentDate())
				.setExpiration(generateExpirationDate()).signWith(SIGNATURE_ALGORITHM, SECRET_KEY).compact();
		return new TokenState(jws, this.EXPIRE_TIME);
	}

	/**
	 * get infomation from a token
	 * 
	 * @param token:
	 *            json web token
	 * @return
	 * @throws TokenExpireTimeException
	 * @throws UnauthorizationException
	 */
	public Claims getClaimsFromToken(String token) {
		Claims claims = null;
		try {
			claims = Jwts.parser().setSigningKey(this.SECRET_KEY).parseClaimsJws(token).getBody();
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return claims;
	}

	/**
	 * 
	 * @param token
	 * @return expiration date
	 */
	private long getExpirationDateFromToken(String token) {
		long expireTime = 0;
		try {
			final Claims claims = this.getClaimsFromToken(token);
			Date validTo = claims.getExpiration();
			Date requestTime = new Date();
			if (validTo.getTime() > requestTime.getTime()) {
				expireTime = (validTo.getTime() - requestTime.getTime()) / 1000;
			} else {
				expireTime = 0;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return expireTime;
	}

	public TokenState getTokenExpire(String token) {
		return new TokenState();
	}

	public String getUsernameFromToken(String token) {
		String username = "";
		try {
			final Claims claims = this.getClaimsFromToken(token);
			if (claims != null) {
				username = claims.getSubject();
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return username;
	}

	private Boolean isTokenExpired(String token) {
		long expiration = this.getExpirationDateFromToken(token);
		return (expiration <= 0);
	}

	public Boolean validateTokenLogin(String token) {
		if (token == null || token.trim().length() == 0) {
			return false;
		}
		String username = getUsernameFromToken(token);

		if (username == null || username.isEmpty()) {
			return false;
		}
		if (isTokenExpired(token)) {
			return false;
		}
		return true;
	}

	private long getCurrentTimeMillis() {
		return System.currentTimeMillis();
	}

	private Date generateCurrentDate() {
		return new Date(getCurrentTimeMillis());
	}

	private Date generateExpirationDate() {
		return new Date(getCurrentTimeMillis() + this.EXPIRE_TIME * 1000);
	}
}
