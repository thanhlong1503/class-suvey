
package com.uet.classsurvey.authentication;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;

import com.uet.classsurvey.authentication.helper.TokenHelper;
import com.uet.classsurvey.service.RoleService;


public class JwtAuthenticationTokenFilter extends UsernamePasswordAuthenticationFilter {

	private final static String TOKEN_HEADER = "Authorization";

	@Autowired
	private TokenHelper tokenHelper;

	@Autowired
	private UserDetailsServiceImpl userDetailService;

	@Autowired
	private RoleService roleService;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest httpRequest = (HttpServletRequest) request;
		String authToken = httpRequest.getHeader(TOKEN_HEADER);
		String url = httpRequest.getRequestURI();
		String method = httpRequest.getMethod();

		// check authentication
		if (tokenHelper.validateTokenLogin(authToken)) {

			String username = tokenHelper.getUsernameFromToken(authToken);
			User user = (User) userDetailService.loadUserByUsername(username);
			if (user != null) {

				String roleCode = null;
				Collection<? extends GrantedAuthority> authorities = user.getAuthorities();
				for (GrantedAuthority grantedAuthority : authorities) {
					roleCode = grantedAuthority.getAuthority();
				}
				// chẹck authorization
				if (this.checkAuthorization(roleCode, url, method)) {

					boolean enabled = true;
					boolean accountNonExpired = true;
					boolean credentialsNonExpired = true;
					boolean accountNonLocked = true;
					UserDetails userDetail = new User(username, user.getPassword(), enabled, accountNonExpired,
							credentialsNonExpired, accountNonLocked, user.getAuthorities());

					UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
							userDetail, null, userDetail.getAuthorities());
					authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpRequest));
					SecurityContextHolder.getContext().setAuthentication(authentication);
				} else {
					UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(null,
							null);
					SecurityContextHolder.getContext().setAuthentication(authentication);
				}
			} else {
				UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(null,
						null);
				SecurityContextHolder.getContext().setAuthentication(authentication);
			}

		} else {
			UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(null, null);
			SecurityContextHolder.getContext().setAuthentication(authentication);
		}

		chain.doFilter(request, response);
	}

	public boolean checkAuthorization(String roleCode, String url, String method) {
	
		return true;
	}
}