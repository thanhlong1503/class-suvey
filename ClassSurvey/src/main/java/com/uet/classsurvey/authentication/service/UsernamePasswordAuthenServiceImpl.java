package com.uet.classsurvey.authentication.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.uet.classsurvey.authentication.core.TokenState;
import com.uet.classsurvey.authentication.core.UserData;
import com.uet.classsurvey.authentication.helper.TokenHelper;
import com.uet.classsurvey.dao.entities.User;
import com.uet.classsurvey.dao.repository.RoleRepository;
import com.uet.classsurvey.dao.repository.UserRepository;
import com.uet.classsurvey.endpoint.dto.auth.LoginDTO;
import com.uet.classsurvey.service.impl.UserServiceImpl;

@Service
public class UsernamePasswordAuthenServiceImpl implements UsernamePasswordAuthenService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	TokenHelper tokenHelper;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

	@Override
	public UserData login(LoginDTO loginRequest) {
		UserData userData = new UserData();
		try {
			User user = userRepository.findByUsername(loginRequest.getUsername());
			if (user != null) {
				// check authentication
				if (passwordEncoder.matches(loginRequest.getPassword(), user.getPassword())) {
					String username = user.getUsername();
					TokenState tokenState = tokenHelper.generateToken(username);

					userData.setTokenState(tokenState);
					userData.setUsername(username);
					return userData;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}
		return null;
	}

}
