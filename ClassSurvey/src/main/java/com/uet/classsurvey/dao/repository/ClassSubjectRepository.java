package com.uet.classsurvey.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.uet.classsurvey.dao.entities.ClassSubject;

public interface ClassSubjectRepository extends JpaRepository<ClassSubject, Integer>, JpaSpecificationExecutor<ClassSubject> {

	public ClassSubject findById(int id);

}
