package com.uet.classsurvey.dao.entities;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.uet.classsurvey.common.util.Constants.ERROR_MESSAGE;

@Entity
@Table(name = "lecturer", catalog = "class_survey", uniqueConstraints = @UniqueConstraint(columnNames = "lecturer_code"))
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Lecturer {

	private static final long serialVersionUID = 1L;
	private int id;
	private String lecturerCode;
	private String fullname;
	private String email;
	private String address;
	private User user;
	private String creator;
	private Date created;
	private String modifier;
	private Date modified;
	private Set<ClassSubject> classSubjects = new HashSet<ClassSubject>(0);

	public Lecturer() {

	}

	public Lecturer(int id, String lecturerCode, User user) {
		super();
		this.id = id;
		this.lecturerCode = lecturerCode;
		this.user = user;
	}

	public Lecturer(String lecturerCode, String fullname, String email, String address) {
		this.lecturerCode = lecturerCode;
		this.fullname = fullname;
		this.email = email;
		this.address = address;
	}

	public Lecturer(int id, String lecturerCode, String fullname, String email, String address, User user,
			String creator, Date created, String modifier, Date modified, Set<ClassSubject> classSubjects) {
		this.id = id;
		this.lecturerCode = lecturerCode;
		this.fullname = fullname;
		this.email = email;
		this.address = address;
		this.user = user;
		this.creator = creator;
		this.created = created;
		this.modifier = modifier;
		this.modified = modified;
		this.classSubjects = classSubjects;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@NotNull(message = ERROR_MESSAGE.NOT_NULL)
	@Size(min = 2, message = "Name should have atleast 2 characters")
	@Column(name = "lecturer_code", unique = true, nullable = false, length = 45)
	public String getLecturerCode() {
		return lecturerCode;
	}

	public void setLecturerCode(String lecturerCode) {
		this.lecturerCode = lecturerCode;
	}

	@Column(name = "fullname", length = 45)
	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	@Column(name = "email", length = 45)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "address", length = 45)
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", nullable = false)
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Column(name = "creator", length = 45)
	public String getCreator() {
		return this.creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created", length = 19)
	public Date getCreated() {
		return this.created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	@Column(name = "modifier", length = 45)
	public String getModifier() {
		return this.modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified", length = 19)
	public Date getModified() {
		return this.modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}

	@JsonIgnore
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "lecturer")
	public Set<ClassSubject> getClassSubjects() {
		return this.classSubjects;
	}

	public void setClassSubjects(Set<ClassSubject> classSubjects) {
		this.classSubjects = classSubjects;
	}

}
