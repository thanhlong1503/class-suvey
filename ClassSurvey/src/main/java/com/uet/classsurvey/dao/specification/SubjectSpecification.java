package com.uet.classsurvey.dao.specification;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.uet.classsurvey.dao.entities.Subject;

public class SubjectSpecification {
	public static Specification<Subject> advanceSearch(Subject subject) {
		return new Specification<Subject>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public Predicate toPredicate(Root<Subject> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<Predicate>();
				Predicate obj = null;

				// search by Subject name
				if (subject.getName() != null && !subject.getName().isEmpty()) {
					obj = criteriaBuilder.like(root.get("name"), "%" + subject.getName() + "%");
					predicates.add(obj);
				}

				// search by subject code
				if (subject.getCode() != null && !subject.getCode().isEmpty()) {
					obj = criteriaBuilder.like(root.get("code"), "%" + subject.getCode() + "%");
					predicates.add(obj);
				}

				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
	}

	public static Specification<Subject> fillterSearch(String flilter) {
		return new Specification<Subject>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public Predicate toPredicate(Root<Subject> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<Predicate>();
				Predicate obj = null;

				if (flilter != null && !flilter.isEmpty()) {
					obj = criteriaBuilder.like(root.get("name"), "%" + flilter + "%");
					predicates.add(obj);

					obj = criteriaBuilder.like(root.get("code"), "%" + flilter + "%");
					predicates.add(obj);

				}

				return criteriaBuilder.or(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
	}

}
