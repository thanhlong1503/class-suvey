package com.uet.classsurvey.dao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.uet.classsurvey.dao.entities.Student;
import com.uet.classsurvey.dao.entities.StudentClassSubject;

public interface StudentClassSubjectRepository
		extends JpaRepository<StudentClassSubject, Integer>, JpaSpecificationExecutor<StudentClassSubject> {

	public StudentClassSubject findById(int id);

	public List<StudentClassSubject> findByStudent(Student student);

}
