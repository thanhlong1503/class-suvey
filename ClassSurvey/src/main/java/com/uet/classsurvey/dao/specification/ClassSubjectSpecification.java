package com.uet.classsurvey.dao.specification;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.uet.classsurvey.dao.entities.ClassSubject;

public class ClassSubjectSpecification {
	public static Specification<ClassSubject> advanceSearch(ClassSubject classSubject) {
		return new Specification<ClassSubject>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public Predicate toPredicate(Root<ClassSubject> root, CriteriaQuery<?> query,
					CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<Predicate>();
				Predicate obj = null;

				// search by ClassSubject code
				if (classSubject.getName() != null && !classSubject.getName().isEmpty()) {
					obj = criteriaBuilder.like(root.get("name"), "%" + classSubject.getName() + "%");
					predicates.add(obj);
				}

				// search by name
				if (classSubject.getYear() != null && !classSubject.getYear().isEmpty()) {
					obj = criteriaBuilder.like(root.get("year"), "%" + classSubject.getYear() + "%");
					predicates.add(obj);
				}

				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
	}

	public static Specification<ClassSubject> fillterSearch(String flilter) {
		return new Specification<ClassSubject>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public Predicate toPredicate(Root<ClassSubject> root, CriteriaQuery<?> query,
					CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<Predicate>();
				Predicate obj = null;

				if (flilter != null && !flilter.isEmpty()) {
					obj = criteriaBuilder.like(root.get("name"), "%" + flilter + "%");
					predicates.add(obj);

				}

				return criteriaBuilder.or(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
	}

}
