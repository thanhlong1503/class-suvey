package com.uet.classsurvey.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.uet.classsurvey.dao.entities.Subject;

public interface SubjectRepository extends JpaRepository<Subject, Integer>, JpaSpecificationExecutor<Subject> {

	public Subject findById(int id);

	public Subject findByName(String name);

	public Subject findByCode(String code);

}
