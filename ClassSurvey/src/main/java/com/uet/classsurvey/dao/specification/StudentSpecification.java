package com.uet.classsurvey.dao.specification;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.uet.classsurvey.dao.entities.Student;

public class StudentSpecification {
	public static Specification<Student> advanceSearch(Student student) {
		return new Specification<Student>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public Predicate toPredicate(Root<Student> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<Predicate>();
				Predicate obj = null;

				// search by student code
				if (student.getStudentCode() != null && !student.getStudentCode().isEmpty()) {
					obj = criteriaBuilder.like(root.get("studentCode"), "%" + student.getStudentCode() + "%");
					predicates.add(obj);
				}

				// search by name
				if (student.getFullname() != null && !student.getFullname().isEmpty()) {
					obj = criteriaBuilder.like(root.get("fullname"), "%" + student.getFullname() + "%");
					predicates.add(obj);
				}

				// search by address
				if (student.getAddress() != null && !student.getAddress().isEmpty()) {
					obj = criteriaBuilder.like(root.get("address"), "%" + student.getAddress() + "%");
					predicates.add(obj);
				}

				// search by email
				if (student.getEmail() != null && !student.getEmail().isEmpty()) {
					obj = criteriaBuilder.like(root.get("email"), "%" + student.getEmail() + "%");
					predicates.add(obj);
				}

				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
	}

	public static Specification<Student> fillterSearch(String flilter) {
		return new Specification<Student>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public Predicate toPredicate(Root<Student> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<Predicate>();
				Predicate obj = null;

				// search by student code
				if (flilter != null && !flilter.isEmpty()) {
					obj = criteriaBuilder.like(root.get("studentCode"), "%" + flilter + "%");
					predicates.add(obj);

					obj = criteriaBuilder.like(root.get("fullname"), "%" + flilter + "%");
					predicates.add(obj);

					obj = criteriaBuilder.like(root.get("address"), "%" + flilter + "%");
					predicates.add(obj);

					obj = criteriaBuilder.like(root.get("email"), "%" + flilter + "%");
					predicates.add(obj);
				}

				return criteriaBuilder.or(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
	}

}
