package com.uet.classsurvey.dao.entities;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "student_class_subject", catalog = "class_survey")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class StudentClassSubject {

	private int id;
	private Student student;
	private ClassSubject classSubject;
	private String creator;
	private Date created;
	private String modifier;
	private Date modified;
	private Survey survey;

	public StudentClassSubject() {

	}

	public StudentClassSubject(int id, Student student, ClassSubject classSubject) {
		this.id = id;
		this.student = student;
		this.classSubject = classSubject;
	}

	public StudentClassSubject(int id, Student student, ClassSubject classSubject, String creator, Date created,
			String modifier, Date modified, Survey survey) {
		super();
		this.id = id;
		this.student = student;
		this.classSubject = classSubject;
		this.creator = creator;
		this.created = created;
		this.modifier = modifier;
		this.modified = modified;
		this.survey = survey;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "student_id", nullable = false)
	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "class_subject_id", nullable = false)
	public ClassSubject getClassSubject() {
		return classSubject;
	}

	public void setClassSubject(ClassSubject classSubject) {
		this.classSubject = classSubject;
	}

	@Column(name = "creator", length = 45)
	public String getCreator() {
		return this.creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created", length = 19)
	public Date getCreated() {
		return this.created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	@Column(name = "modifier", length = 45)
	public String getModifier() {
		return this.modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified", length = 19)
	public Date getModified() {
		return this.modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}

	@JsonIgnoreProperties("studentClassSubject")
	@OneToOne(fetch = FetchType.LAZY, mappedBy = "studentClassSubject")
	public Survey getSurvey() {
		return survey;
	}

	public void setSurvey(Survey survey) {
		this.survey = survey;
	}

}
