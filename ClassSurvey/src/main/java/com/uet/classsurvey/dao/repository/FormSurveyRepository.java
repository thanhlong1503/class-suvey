package com.uet.classsurvey.dao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.uet.classsurvey.dao.entities.FormSurvey;

public interface FormSurveyRepository extends JpaRepository<FormSurvey, Integer>, JpaSpecificationExecutor<FormSurvey> {

	public FormSurvey findById(int id);

	public List<FormSurvey> findByIsDefault(int id);

}
