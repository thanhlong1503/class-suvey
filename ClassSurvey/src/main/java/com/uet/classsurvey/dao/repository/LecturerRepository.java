package com.uet.classsurvey.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.uet.classsurvey.dao.entities.Lecturer;

public interface LecturerRepository extends JpaRepository<Lecturer, Integer>, JpaSpecificationExecutor<Lecturer> {

	public Lecturer findById(int id);

	public Lecturer findByLecturerCode(String lecturerCode);

}
