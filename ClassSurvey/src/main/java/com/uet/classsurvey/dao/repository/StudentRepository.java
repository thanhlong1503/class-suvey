package com.uet.classsurvey.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.uet.classsurvey.dao.entities.Student;

public interface StudentRepository extends JpaRepository<Student, Integer>, JpaSpecificationExecutor<Student> {

	public Student findById(int id);

	public Student findByStudentCode(String studentCode);

}
