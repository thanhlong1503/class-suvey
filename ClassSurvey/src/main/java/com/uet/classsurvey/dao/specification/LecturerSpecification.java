package com.uet.classsurvey.dao.specification;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.uet.classsurvey.dao.entities.Lecturer;

public class LecturerSpecification {
	public static Specification<Lecturer> advanceSearch(Lecturer lecturer) {
		return new Specification<Lecturer>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public Predicate toPredicate(Root<Lecturer> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<Predicate>();
				Predicate obj = null;

				// search by Lecturer code
				if (lecturer.getLecturerCode() != null && !lecturer.getLecturerCode().isEmpty()) {
					obj = criteriaBuilder.like(root.get("lecturerCode"), "%" + lecturer.getLecturerCode() + "%");
					predicates.add(obj);
				}

				// search by name
				if (lecturer.getFullname() != null && !lecturer.getFullname().isEmpty()) {
					obj = criteriaBuilder.like(root.get("fullname"), "%" + lecturer.getFullname() + "%");
					predicates.add(obj);
				}

				// search by address
				if (lecturer.getAddress() != null && !lecturer.getAddress().isEmpty()) {
					obj = criteriaBuilder.like(root.get("address"), "%" + lecturer.getAddress() + "%");
					predicates.add(obj);
				}

				// search by email
				if (lecturer.getEmail() != null && !lecturer.getEmail().isEmpty()) {
					obj = criteriaBuilder.like(root.get("email"), "%" + lecturer.getEmail() + "%");
					predicates.add(obj);
				}

				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
	}

	public static Specification<Lecturer> fillterSearch(String flilter) {
		return new Specification<Lecturer>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public Predicate toPredicate(Root<Lecturer> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<Predicate>();
				Predicate obj = null;

				// search by Lecturer code
				if (flilter != null && !flilter.isEmpty()) {
					obj = criteriaBuilder.like(root.get("lecturerCode"), "%" + flilter + "%");
					predicates.add(obj);

					obj = criteriaBuilder.like(root.get("fullname"), "%" + flilter + "%");
					predicates.add(obj);

					obj = criteriaBuilder.like(root.get("address"), "%" + flilter + "%");
					predicates.add(obj);

					obj = criteriaBuilder.like(root.get("email"), "%" + flilter + "%");
					predicates.add(obj);
				}

				return criteriaBuilder.or(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
	}

}
