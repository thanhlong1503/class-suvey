package com.uet.classsurvey.dao.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "survey", catalog = "class_survey")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Survey {

	private int id;
	private StudentClassSubject studentClassSubject;
	private FormSurvey formSurvey;
	private String data;
	private String creator;
	private Date created;
	private String modifier;
	private Date modified;

	public Survey() {

	}

	public Survey(int id, StudentClassSubject studentClassSubject, FormSurvey formSurvey) {
		this.id = id;
		this.studentClassSubject = studentClassSubject;
		this.formSurvey = formSurvey;
	}

	public Survey(int id, StudentClassSubject studentClassSubject, FormSurvey formSurvey, String data, String creator,
			Date created, String modifier, Date modified) {
		super();
		this.id = id;
		this.studentClassSubject = studentClassSubject;
		this.formSurvey = formSurvey;
		this.data = data;
		this.creator = creator;
		this.created = created;
		this.modifier = modifier;
		this.modified = modified;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@JsonIgnoreProperties("survey")
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "student_class_subject_id", nullable = false)
	public StudentClassSubject getStudentClassSubject() {
		return studentClassSubject;
	}

	public void setStudentClassSubject(StudentClassSubject studentClassSubject) {
		this.studentClassSubject = studentClassSubject;
	}

	@JsonIgnoreProperties("surveys")
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "form_survey_id", nullable = false)
	public FormSurvey getFormSurvey() {
		return formSurvey;
	}

	public void setFormSurvey(FormSurvey formSurvey) {
		this.formSurvey = formSurvey;
	}

	@Column(name = "filed_data", length = 2000)
	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	@Column(name = "creator", length = 45)
	public String getCreator() {
		return this.creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created", length = 19)
	public Date getCreated() {
		return this.created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	@Column(name = "modifier", length = 45)
	public String getModifier() {
		return this.modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified", length = 19)
	public Date getModified() {
		return this.modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}

}
