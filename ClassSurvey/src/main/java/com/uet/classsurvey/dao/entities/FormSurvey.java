package com.uet.classsurvey.dao.entities;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.uet.classsurvey.common.util.Constants.ERROR_MESSAGE;

@Entity
@Table(name = "form_survey", catalog = "class_survey", uniqueConstraints = @UniqueConstraint(columnNames = "code"))
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class FormSurvey {

	private int id;
	private String code;
	private String name;
	private String fieldData;
	private int isDefault;
	private String creator;
	private Date created;
	private String modifier;
	private Date modified;
	private Set<Survey> surveys = new HashSet<Survey>(0);

	public FormSurvey() {

	}

	public FormSurvey(int id, String code) {
		this.id = id;
		this.code = code;
	}

	public FormSurvey(int id, String code, String name, String fieldData, int isDefault, String creator, Date created,
			String modifier, Date modified, Set<Survey> surveys) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.fieldData = fieldData;
		this.isDefault = isDefault;
		this.creator = creator;
		this.created = created;
		this.modifier = modifier;
		this.modified = modified;
		this.surveys = surveys;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@NotEmpty(message = ERROR_MESSAGE.NOT_EMPTY)
	@Column(name = "code", unique = true, nullable = false, length = 45)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@NotEmpty(message = ERROR_MESSAGE.NOT_EMPTY)
	@Column(name = "name", nullable = false, length = 100)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "filed_data", length = 200)
	public String getFieldData() {
		return fieldData;
	}

	public void setFieldData(String fieldData) {
		this.fieldData = fieldData;
	}

	@Column(name = "is_default")
	public int getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(int isDefault) {
		this.isDefault = isDefault;
	}

	@Column(name = "creator", length = 45)
	public String getCreator() {
		return this.creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created", length = 19)
	public Date getCreated() {
		return this.created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	@Column(name = "modifier", length = 45)
	public String getModifier() {
		return this.modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified", length = 19)
	public Date getModified() {
		return this.modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}

	@JsonIgnoreProperties("formSurvey")
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "formSurvey")
	public Set<Survey> getSurveys() {
		return surveys;
	}

	public void setSurveys(Set<Survey> surveys) {
		this.surveys = surveys;
	}

}
