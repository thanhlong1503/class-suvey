package com.uet.classsurvey.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.uet.classsurvey.dao.entities.ClassSubject;
import com.uet.classsurvey.dao.entities.Lecturer;
import com.uet.classsurvey.dao.entities.Student;
import com.uet.classsurvey.dao.entities.Subject;

public class JsonHelper {

	private static final Logger LOGGER = LoggerFactory.getLogger(JsonHelper.class);

	public static Gson gson = new Gson();

	public static <T> T convertToObject(String json, Class<T> classOfT) {

		T object = null;
		try {
			object = gson.fromJson(json, classOfT);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
		}

		return object;

	}

}
