package com.uet.classsurvey.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

public class Constants {
	
	public final class REGEX{
		public static final String EMAIL_ADDRESS_REGEX = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
	}
	
	public final class ERROR_MESSAGE{
		public static final String NOT_NULL = "Must not be null";
		public static final String NOT_EMPTY = "Must not be empty";
		public static final String LENG_USERNAME = "Password should have atleast 2 characters";
		public static final String LENG_PASSWORD = "Password should have atleast 5 characters";
	}
	
	public final class RESPONSE_CODE {
		
		public static final int SUCCESS_CODE = 1;
		public static final int ERR_CODE_UNAUTHORIZED = 401;
		public static final int ERR_CODE_BAD_REQUEST = 400;
		public static final int ERR_CODE_FORBIDDEN = 403;
		public static final int ERR_CODE_NOT_FOUND = 404;
		public static final int ERR_CODE_METHOD_NOT_ALLOW = 405;

	}

	public final class RESPONSE_MESSAGE {
		public static final String SUCCESS_MSG = "Success";
		public static final String ERR_MSG_UNAUTHORIZED = "Unauthorized";
		public static final String MSG_ERROR = "Error ";
		
		public static final String ERR_MSG_BAD_REQUEST = "Bad request";
		public static final String ERR_MSG_FORBIDDEN = "Forbidden";
		public static final String ERR_MSG_NOT_FOUND = "Api not found";
		public static final String ERR_MSG_METHOD_NOT_ALLOW = "Method not allow";
	}
	
	public final class TRANSACTION_STATUS {
		public static final int INPUT = 1;
		public static final int OUTPUT = 2;
		public static final int REFUND = 3;
	}
	
	
	public static long getCurrentTimeMillis() {
		return System.currentTimeMillis();
	}

	// get now time
	public static Date getNowTime() {
		Date date = new Date();
		return date;
	}
	
	public static String getCurrentDate() {
		return new SimpleDateFormat("dd-MM-yyyy").format(new Date());
	}

	// get current user
	public static String getCurrentUser() {
		try {
			Object principal = getAuthorization().getPrincipal();
			if (principal instanceof User) {
				User user = (User) principal;
				return user.getUsername();
			} else {
				return principal.toString();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "";
	}

	public static Authentication getAuthorization() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return authentication;
	}

	// conver int value to Integer object
	public static Integer convertIntToInteger(int input) {

		return new Integer(input);
	}
	
	public static Date convertStringToDate(String dateStr) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		Date date = null;
		try {

            date = formatter.parse(dateStr);
            System.out.println(date);
            System.out.println(formatter.format(date));

        } catch (ParseException e) {
            e.printStackTrace();
        }
		return date;
	}

}
